#use wml::debian::translation-check translation="b6e1b7f61abd59aa2dfcb8494afd20f6146915e4"
<define-tag pagetitle>Debian Edu / Skolelinux Bullseye: una solución Linux completa para su centro escolar</define-tag>
<define-tag release_date>2021-08-15</define-tag>
#use wml::debian::news

<p>
¿Se encarga de administrar un laboratorio de informática o la red de un centro escolar completo?
¿Le gustaría instalar servidores, estaciones de trabajo y ordenadores portátiles para que
trabajen conjuntamente?
¿Desea la estabilidad de Debian con servicios de red ya
configurados?
¿Le gustaría tener una herramienta web para gestionar sistemas y varios
cientos de cuentas de usuario, o incluso más?
¿Se ha preguntado si las computadoras antiguas pueden ser útiles y cómo?
</p>

<p>
Entonces Debian Edu es para usted. Los propios profesores y profesoras o su personal de soporte técnico
pueden implementar en pocos días un entorno de estudio multiusuario y
multicomputadora. Debian Edu viene con cientos de aplicaciones preinstaladas,
y siempre puede añadir más paquetes de Debian.
</p>

<p>
El equipo de desarrollo de Debian Edu se complace en anunciar Debian Edu 11
<q>Bullseye</q>, la versión de Debian Edu / Skolelinux basada
en Debian 11 <q>Bullseye</q>.
Por favor, considere probarlo e informarnos del resultado (&lt;debian-edu@lists.debian.org&gt;)
para ayudarnos a mejorarlo.
</p>

<h2>Acerca de Debian Edu y Skolelinux</h2>

<p>
<a href="https://wiki.debian.org/DebianEdu">Debian Edu, también conocido como
Skolelinux</a>, es una distribución Linux basada en Debian que proporciona un
entorno de red listo para usar y completamente configurado para un centro escolar.
Inmediatamente tras la instalación se encuentra operativo un servidor ejecutando
todos los servicios necesarios para una red escolar, a la espera de que se añadan
usuarios y máquinas vía GOsa², una cómoda interfaz web.
También queda preparado un entorno de arranque desde red de forma que, tras la
instalación del servidor principal desde CD / DVD / BD o medio extraíble USB, el resto de
máquinas se pueden instalar por red.
Se pueden utilizar computadoras antiguas (de hasta diez años de antigüedad o así) como clientes
ligeros LTSP o como estaciones de trabajo sin disco, cargando el sistema operativo por red sin
que sean necesarias ninguna instalación ni configuración en absoluto.
El servidor escolar de Debian Edu proporciona una base de datos LDAP y un servicio
de autenticación Kerberos, directorios personales («home») centralizados, un servidor DHCP, un
proxy web y muchos otros servicios. El entorno de escritorio contiene más de 70
paquetes de software educativo, y hay más disponibles en el archivo de Debian.
Los centros escolares pueden elegir entre los entornos de escritorio Xfce, GNOME, LXDE,
MATE, KDE Plasma, Cinnamon y LXQt.
</p>

<h2>Novedades en Debian Edu 11 <q>Bullseye</q></h2>

<p>Estos son algunos puntos de las notas de publicación de Debian Edu 11 <q>Bullseye</q>,
basado en Debian 11 <q>Bullseye</q>.
La lista completa, que incluye información más detallada, forma parte del
<a href="https://wiki.debian.org/DebianEdu/Documentation/Bullseye/Features#New_features_in_Debian_Edu_Bullseye">capitulo del manual de Debian Edu</a> correspondiente.
</p>

<ul>
<li>
Nuevo <a href="https://ltsp.org">LTSP</a> para soportar estaciones de trabajo sin disco. Los clientes
ligeros todavía están soportados, ahora mediante la tecnología
<a href="https://wiki.x2go.org">X2Go</a>.
</li>
<li>
El arranque desde red se proporciona a través de iPXE, en lugar de PXELINUX, para
cumplir con LTSP.
</li>
<li>
Para las instalaciones iPXE se usa el modo gráfico del instalador de Debian.
</li>
<li>
Samba ahora se configura como <q>servidor independiente</q> con soporte de SMB2/SMB3.
</li>
<li>
DuckDuckGo es el buscador por omisión tanto para Firefox ESR como para Chromium.
</li>
<li>
Añadida nueva herramienta para configurar freeRADIUS con soporte de los métodos
EAP-TTLS/PAP y PEAP-MSCHAPV2.
</li>
<li>
Disponible herramienta mejorada para configurar un sistema nuevo con perfil <q>Minimal</q> como
puerta de enlace dedicada.
</li>
</ul>

<h2>Opciones de descarga, pasos de instalación y manual</h2>

<p>
Hay disponibles imágenes de CD oficiales del instalador por red Debian para PC
de 64 y de 32 bits. La imagen de 32 bits se necesitará en raras ocasiones (para
PC de más de unos quince años). Las imágenes se pueden descargar de las ubicaciones
siguientes:
</p>
<ul>
<li>
<url http://get.debian.org/cdimage/release/current/amd64/iso-cd/>
</li>
<li>
<url http://get.debian.org/cdimage/release/current/i386/iso-cd/>
</li>
</ul>

<p>
Alternativamente, también hay disponibles imágenes Debian oficiales de BD (de
más de 5 GB). Es posible configurar una red Debian Edu completa sin
conexión a Internet (incluyendo todos los entornos de escritorio y la configuración regional para todos
los idiomas soportados por Debian).
Estas imágenes se pueden descargar de las ubicaciones siguientes:
</p>

<ul>
<li>
<url http://get.debian.org/cdimage/release/current/amd64/iso-bd/>
</li>
<li>
<url http://get.debian.org/cdimage/release/current/i386/iso-bd/>
</li>
</ul>

<p>
Las imágenes se pueden verificar utilizando las sumas de verificación («checksums») firmadas disponibles en el directorio
de descarga.
<br />
Una vez descargada una imagen, puede comprobar que:
</p>
<ul>
<li>
su suma de verificación coincide con la indicada en el fichero de verificación; y que
</li>
<li>
el fichero de verificación no ha sido alterado.
</li>
</ul>
<p>
Para más información sobre cómo hacerlo, lea la
<a href="https://www.debian.org/CD/verify">guía de verificación</a>.
</p>

<p>
Debian Edu 11 <q>Bullseye</q> está basado completamente en Debian 11 <q>Bullseye</q>;
por lo tanto, en el archivo Debian están disponibles los fuentes de todos los paquetes.
</p>

<p>
Tenga en cuenta la
<a href="https://wiki.debian.org/DebianEdu/Status/Bullseye">página de estado de Debian Edu Bullseye</a>
para información siempre actualizada sobre Debian Edu 11 <q>Bullseye</q>, incluyendo
instrucciones para descargar las imágenes ISO con <code>rsync</code>.
</p>

<p>
Para actualizaciones desde Debian Edu 10 <q>Buster</q> lea el
<a href="https://wiki.debian.org/DebianEdu/Documentation/Bullseye/Upgrades">capítulo del manual de Debian Edu</a> correspondiente.
</p>

<p>
Para notas de instalación lea el
<a href="https://wiki.debian.org/DebianEdu/Documentation/Bullseye/Installation#Installing_Debian_Edu">capítulo del manual de Debian Edu</a> correspondiente.
</p>

<p>
Después de la instalación tiene que realizar estos
<a href="https://wiki.debian.org/DebianEdu/Documentation/Bullseye/GettingStarted">primeros pasos</a>.
</p>

<p>
Consulte la <a href="https://wiki.debian.org/DebianEdu/Documentation/Bullseye/">wiki de Debian Edu</a>
para acceder a la versión más reciente en inglés del manual de Debian Edu <q>Bullseye</q>.
El manual ha sido traducido en su integridad al alemán, francés, italiano, danés, holandés,
noruego bokmål, japonés, chino simplificado y portugués (Portugal).
También hay versiones traducidas parcialmente al español, rumano y polaco.
Tiene disponibles <a href="https://jenkins.debian.net/userContent/debian-edu-doc/">
las últimas versiones traducidas del manual</a>.
</p>

<p>
En las notas de publicación y en el manual de instalación hay más información sobre
el propio Debian 11 <q>Bullseye</q>; vea <a href="$(HOME)/">https://www.debian.org/</a>.
</p>

<h2>Acerca de Debian</h2>

<p>El proyecto Debian es una asociación de desarrolladores de software libre que
aportan de forma voluntaria su tiempo y esfuerzo para producir el sistema operativo
Debian, un sistema operativo completamente libre.</p>

<h2>Información de contacto</h2>

<p>Para más información, visite las páginas web de Debian en
<a href="$(HOME)/">https://www.debian.org/</a> o envíe un correo electrónico a
&lt;press@debian.org&gt;.</p>
