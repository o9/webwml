#use wml::debian::template title="Versiones de Debian"
#use wml::debian::translation-check translation="2b2b2d98876137a0efdabdfc2abad6088d4c511f" maintainer="Javier Fernández-Sanguino"
#include "$(ENGLISHDIR)/releases/info"
 
<p>Debian siempre mantiene al menos tres versiones en mantenimiento
activo: <q>estable</q> (<q>stable</q>), <q>en pruebas</q> (<q>testing</q>) e
<q>inestable</q> (<q>unstable</q>).

<dl>
<dt><a href="stable/">estable</a></dt>
  <dd>
  <p>
    La distribución <q>estable</q> contiene la publicación oficial más
  reciente de Debian.
  </p>
<p>
  Esta es la versión de producción de Debian, cuyo uso recomendamos
  principalmente.
</p>
<p>
  La versión <q>estable</q> actual de Debian es la
  <:=substr '<current_initial_release>', 0, 2:>, llamada <em><current_release_name></em>.
<ifeq "<current_initial_release>" "<current_release>"
  "Fue publicada el <current_release_date>."
/>
<ifneq "<current_initial_release>" "<current_release>"
  "Fue publicada originalmente con la versión <current_initial_release>
  el <current_initial_release_date> y su última
  actualización es la versión <current_release>, publicada el <current_release_date>."
/>
</p>
</dd>

<dt><a href="testing/">en pruebas</a></dt>
  <dd>
  <p>
  La distribución <q>en pruebas</q> (<q>testing</q>) contiene paquetes que aún
  no han sido aceptados en la rama <q>estable</q>, pero están a la espera
  de ello. La principal ventaja de usar esta distribución es que tiene
  versiones más recientes del software.
  </p> 
  <p>
  Vea las <a href="$(DOC)/manuals/debian-faq/">Preguntas frecuentes de Debian</a> si desea más información
  sobre <a href="$(DOC)/manuals/debian-faq/ftparchives#testing">qué es <q>pruebas</q></a>
  y <a href="$(DOC)/manuals/debian-faq/ftparchives#frozen">cómo se convierte en
  <q>estable</q></a>.
</p>
<p>
  La distribución actual de <q>en pruebas</q> es <em><current_testing_name></em>.
</p>
</dd>

<dt><a href="unstable/">inestable</a>
  <dd>
  <p>
  La distribución <q>inestable</q> es donde tiene lugar el desarrollo activo
  de Debian. Generalmente, esta distribución es la que usan los
  desarrolladores y aquellos que quieren estar a la última. Es recomendable que las personas
que usan <q>inestable</q> se suscriban a la lista de correo debian-devel-announce
para recibir notificaciones de los cambios importantes, por ejemplo sobre
actualizaciones que pueden fallar.
</p>
<p>
  La distribución <q>inestable</q> siempre se llama <em>sid</em>.
</p>
</dd>
</dl>

<h2>Ciclo de vida de las versiones</h2>
<p>
Debian anuncia su nueva versión estable de manera regular. Los usuarios pueden
esperar unos 3 años de soporte completo para cada versión, y 2 años de soporte
extra «LTS».
</p>
<p>
Consulte la página wiki <a href="https://wiki.debian.org/DebianReleases">Debian Releases</a>
y <a href="https://wiki.debian.org/LTS">Debian LTS</a> para información detallada.
</p>


<h2>Índice de versiones</h2>

<ul>

  <li><a href="<current_testing_name>/">La siguiente versión de Debian se llama
    <q><current_testing_name></q></a>
     &mdash; no se ha establecido aún la fecha de publicación.
  </li>

  <li><a href="bullseye/">Debian 11 (<q>bullseye</q>)</a>
      &mdash; publicación <q>estable</q> actual
  </li>

  <li><a href="buster/">Debian 10 (<q>buster</q>)</a>
      &mdash; publicación <q>estable antigua</q> actual
  </li>

  <li><a href="stretch/">Debian 9 (<q>stretch</q>)</a>
     &mdash; versión </q>estable más antigua antigua</q>, bajo <a href="https://wiki.debian.org/LTS">soporte a largo plazo («LTS»)</a>.
  </li>

  <li><a href="jessie/">Debian 8 (<q>jessie</q>)</a>
      &mdash; versión archivada, bajo  <a href="https://wiki.debian.org/LTS/Extended">soporte «LTS» extendido</a>.
  </li>
  
  <li><a href="wheezy/">Debian 7 (<q>wheezy</q>)</a>
      &mdash; versión estable obsoleta.
  </li>

  <li><a href="squeeze/">Debian 6.0 (<q>squeeze</q>)</a>
      &mdash; versión estable obsoleta.
  </li>
  <li><a href="lenny/">Debian GNU/Linux 5.0 (<q>lenny</q>)</a>
      &mdash; versión estable obsoleta.
  </li>
  <li><a href="etch/">Debian GNU/Linux 4.0 (<q>etch</q>)</a>
      &mdash; versión estable obsoleta.
  </li>
  <li><a href="sarge/">Debian GNU/Linux 3.1 (<q>sarge</q>)</a>
      &mdash; versión estable obsoleta.
  </li>
  <li><a href="woody/">Debian GNU/Linux 3.0 (<q>woody</q>)</a>
      &mdash; versión estable obsoleta.
  </li>
  <li><a href="potato/">Debian GNU/Linux 2.2 (<q>potato</q>)</a>
      &mdash; versión estable obsoleta.
  </li>
  <li><a href="slink/">Debian GNU/Linux 2.1 (<q>slink</q>)</a>
      &mdash; versión estable obsoleta.
  </li>
  <li><a href="hamm/">Debian GNU/Linux 2.0 (<q>hamm</q>)</a>
      &mdash; versión estable obsoleta.
  </li>
</ul>

<p>Las páginas web de las versiones obsoletas de Debian se mantienen
intactas, pero las distribuciones en sí sólo puede encontrarlas en un
<a href="$(HOME)/distrib/archive">archivo</a> aparte.</p>

<p>Consulte las <a href="$(HOME)/doc/manuals/debian-faq/">Preguntas frecuentes de Debian</a> si desea una
explicación de 
<a href="$(HOME)/doc/manuals/debian-faq/ftparchives#sourceforcodenames">dónde
surgieron estos nombres en clave.</a></p>

<h2>Integridad de los datos en las versiones</h2>

<p>Se garantiza la integridad de los datos mediante un fichero
<code>Release</code> firmado digitalmente. Para asegurar que todos los
ficheros de la versión pertenecen a ella, se copian sumas de comprobación de
todos los ficheros <code>Packages</code> en el fichero.</p>

<p>Las firmas digitales de este fichero se almacenan en el archivo
<code>Release.gpg</code>, utilizando la versión actual de la clave de firmado 
del repositorio. Para <q>estable</q> y <q>antigua estable</q> se genera una firma
adicional con una clave que no está conectada a la red y generada específicamente
para esa versión por un miembro del <a
href="$(HOME)/intro/organization#release-team">Equipo responsable de la
publicación de la versión estable</a>.</p>
