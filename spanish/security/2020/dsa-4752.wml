#use wml::debian::translation-check translation="5346ad19e1bb39a2123f70e49de6fe4ffa9caa5b"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se descubrieron varias vulnerabilidades en BIND, una implementación
de servidor DNS.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8619">CVE-2020-8619</a>

    <p>Se descubrió que un asterisco en una entrada no
    terminal puede provocar un fallo de aserción, dando lugar a denegación
    de servicio.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8622">CVE-2020-8622</a>

    <p>Dave Feldman, Jeff Warren y Joel Cunningham informaron de que una
    respuesta TSIG truncada puede provocar un fallo de aserción, dando lugar
    a denegación de servicio.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8623">CVE-2020-8623</a>

    <p>Lyu Chiy informó de que un defecto en el código de PKCS#11 nativo puede provocar
    un fallo de aserción susceptible de ser desencadenado de forma remota, dando lugar a denegación
    de servicio.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8624">CVE-2020-8624</a>

    <p>Joop Boonen informó de que las reglas de update-policy del tipo <q>subdomain</q>
    se aplican incorrectamente, lo que permite actualizar todas las partes de la zona
    además del subdominio que se pretende actualizar.</p></li>

</ul>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 1:9.11.5.P4+dfsg-5.1+deb10u2.</p>

<p>Le recomendamos que actualice los paquetes de bind9.</p>

<p>Para información detallada sobre el estado de seguridad de bind9, consulte su página
en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/bind9">https://security-tracker.debian.org/tracker/bind9</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4752.data"
