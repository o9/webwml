#use wml::debian::countries
#use wml::debian::url
#use wml::debian::common_tags
 
# Here is the code of the image carousel (using the tags in index.def file)

<!-- The following div is containing the static image carousel.
     Currently made of five images. -->
<div class="wrapper">
  <input checked type="radio" name="slider" id="slide1" />
  <input type="radio" name="slider" id="slide2" />
  <input type="radio" name="slider" id="slide3" />
  <input type="radio" name="slider" id="slide4" />
  <input type="radio" name="slider" id="slide5" />

  <div class="slider-wrapper">
    <div class="inner">

     <article>
      <div class="info top-left">
       <h3><MiniDebConfRegensburgTitle></h3>
      </div>
      <img src="Pics//mini-dc21-regensburg.jpg" alt="<MiniDebConfRegensburgAlt>"/>
     </article>

     <article>
      <div class="info bottom-right">
       <h3><DC19Title></h3>
      </div>
      <img src="Pics/Debconf19_group_photo.jpg" alt="<DC19Alt>"/>
     </article>

     <article>
      <div class="info top-left">
       <h3><CalamaresTitle></h3>
      </div>
      <img src="Pics/calamares-bullseye.png" alt="<CalamaresAlt>" />
     </article>

     <article>
      <div class="info top-left">
       <h3><SwissKnifeTitle></h3>
      </div>
      <img src="Pics/debian-swiss-knife-hands-1024x576.jpg" alt="<SwissKnifeAlt>" />
     </article>

     <article>
      <div class="info top-left">
       <h3><PeopleTitle></h3>
      </div>
      <img src="Pics/debian-funny-people-1024x576.jpg" alt="<PeopleAlt>"/>
     </article>
    </div>  <!-- .inner -->
  </div>  <!-- .slider-wrapper -->

  <div class="slider-prev-next-control">
   <label for="slide1"></label>
   <label for="slide2"></label>
   <label for="slide3"></label>
   <label for="slide4"></label>
   <label for="slide5"></label>
  </div>  <!-- .slider-prev-next-control -->

  <div class="slider-dot-control">
   <label for="slide1"></label>
   <label for="slide2"></label>
   <label for="slide3"></label>
   <label for="slide4"></label>
   <label for="slide5"></label>
  </div>  <!-- .slider-dot-control -->
 </div>
</div>
