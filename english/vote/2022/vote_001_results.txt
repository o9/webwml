Starting results calculation at Sun Mar 27 00:00:15 2022

Option 1 "Hide identities of Developers casting a particular vote"
Option 2 "Hide identities of Developers casting a particular vote and allow verification"
Option 3 "Reaffirm public voting"
Option 4 "None of the above"

In the following table, tally[row x][col y] represents the votes that
option x received over option y.

                  Option
              1     2     3     4 
            ===   ===   ===   === 
Option 1           72   114   149 
Option 2    144         142   185 
Option 3    137   107         163 
Option 4     94    61    68       



Looking at row 2, column 1, Hide identities of Developers casting a particular vote and allow verification
received 144 votes over Hide identities of Developers casting a particular vote

Looking at row 1, column 2, Hide identities of Developers casting a particular vote
received 72 votes over Hide identities of Developers casting a particular vote and allow verification.

Option 1 Reached quorum: 149 > 47.9765567751584
Option 2 Reached quorum: 185 > 47.9765567751584
Option 3 Reached quorum: 163 > 47.9765567751584


Dropping Option 1 because of Majority. (1.585106382978723404255319148936170212766)  1.585 (149/94) < 3
Option 2 passes Majority.               3.033 (185/61) >= 3
Option 3 passes Majority.               2.397 (163/68) > 1


  Option 2 defeats Option 3 by ( 142 -  107) =   35 votes.
  Option 2 defeats Option 4 by ( 185 -   61) =  124 votes.
  Option 3 defeats Option 4 by ( 163 -   68) =   95 votes.


The Schwartz Set contains:
	 Option 2 "Hide identities of Developers casting a particular vote and allow verification"



-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

The winners are:
	 Option 2 "Hide identities of Developers casting a particular vote and allow verification"

-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

The total numbers of votes tallied = 258
