<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>User <q>Arminius</q> discovered a vulnerability in Vim, an enhanced version of the
standard UNIX editor Vi (Vi IMproved), which also affected the Neovim fork, an
extensible editor focused on modern code and features:</p>
  
<p>Editors typically provide a way to embed editor configuration commands (aka
modelines) which are executed once a file is opened, while harmful commands
are filtered by a sandbox mechanism. It was discovered that the <q>source</q>
command (used to include and execute another file) was not filtered, allowing
shell command execution with a carefully crafted file opened in Neovim.</p>

<p>For the oldstable distribution (stretch), this problem has been fixed
in version 0.1.7-4+deb9u1.</p>

<p>We recommend that you upgrade your neovim packages.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4487.data"
# $Id: $
