<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that the default blacklist of XStream, a Java library
to serialise objects to XML and back again, was vulnerable to the
execution of arbitrary shell commands by manipulating the processed
input stream.</p>

<p>For additional defense-in-depth it is recommended to switch to the
whitelist approach of XStream's security framework. For additional
information please refer to
<a href="https://github.com/x-stream/xstream/security/advisories/GHSA-mw36-7c6c-q4q2">\
https://github.com/x-stream/xstream/security/advisories/GHSA-mw36-7c6c-q4q2</a></p>

<p>For the stable distribution (buster), this problem has been fixed in
version 1.4.11.1-1+deb10u1.</p>

<p>We recommend that you upgrade your libxstream-java packages.</p>

<p>For the detailed security status of libxstream-java please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libxstream-java">\
https://security-tracker.debian.org/tracker/libxstream-java</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4811.data"
# $Id: $
