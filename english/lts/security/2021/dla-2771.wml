<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were fixed in MIT Kerberos,
a system for authenticating users and services on a network.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5729">CVE-2018-5729</a>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5730">CVE-2018-5730</a>

    <p>Fix flaws in LDAP DN checking.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20217">CVE-2018-20217</a>

    <p>Ignore password attributes for S4U2Self requests.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-37750">CVE-2021-37750</a>

    <p>Fix KDC null deref on TGS inner body null server.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1.15-1+deb9u3.</p>

<p>We recommend that you upgrade your krb5 packages.</p>

<p>For the detailed security status of krb5 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/krb5">https://security-tracker.debian.org/tracker/krb5</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2771.data"
# $Id: $
