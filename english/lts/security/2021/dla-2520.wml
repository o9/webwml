<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>There was an integer overflow vulnerability concerning the length of websocket
frames received via a websocket connection. An attacker could use this flaw to
cause a denial of service attack on an HTTP Server allowing websocket
connections.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.1.0-1+deb9u1.</p>

<p>We recommend that you upgrade your golang-websocket packages.</p>

<p>For the detailed security status of golang-websocket please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/golang-websocket">https://security-tracker.debian.org/tracker/golang-websocket</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2520.data"
# $Id: $
