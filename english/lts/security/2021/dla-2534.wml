<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The Qualys Research Labs discovered a heap-based buffer overflow
vulnerability in sudo, a program designed to provide limited super user
privileges to specific users. Any local user (sudoers and non-sudoers)
can exploit this flaw for root privilege escalation.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.8.19p1-2.1+deb9u3.</p>

<p>We recommend that you upgrade your sudo packages.</p>

<p>For the detailed security status of sudo please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/sudo">https://security-tracker.debian.org/tracker/sudo</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2534.data"
# $Id: $
