<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This update ships updated CPU microcode for some types of Intel CPUs
and provides mitigations for security vulnerabilities which could
result in privilege escalation in combination with VT-d and various
side channel attacks.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
3.20210608.2~deb9u2.</p>

<p>Please note that one of the processors is not receiving this update
and so the users of 0x906ea processors that don't have Intel Wireless
on-board can use the package from the buster-security, instead.</p>

<p>We recommend that you upgrade your intel-microcode packages.</p>

<p>For the detailed security status of intel-microcode please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/intel-microcode">https://security-tracker.debian.org/tracker/intel-microcode</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2718.data"
# $Id: $
