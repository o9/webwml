<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Jan-Niklas Sohn discovered that there was an input validation failure in the
X.Org display server.</p>

<p>Insufficient checks on the lengths of the XInput extension's
ChangeFeedbackControl request could have lead to out of bounds memory accesses
in the X server. These issues can lead to privilege escalation for authorised
clients, particularly on systems where the X server is running as a privileged
user.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3472">CVE-2021-3472</a>

    <p>Fix XChangeFeedbackControl() request underflow</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
2:1.19.2-1+deb9u8.</p>

<p>We recommend that you upgrade your xorg-server packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2627.data"
# $Id: $
