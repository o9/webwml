<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several issues were found in Poppler, a PDF rendering library, that could
lead to denial of service or possibly other unspecified impact when
processing maliciously crafted documents.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
0.48.0-2+deb9u3.</p>

<p>We recommend that you upgrade your poppler packages.</p>

<p>For the detailed security status of poppler please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/poppler">https://security-tracker.debian.org/tracker/poppler</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2287.data"
# $Id: $
