<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p><a href="https://security-tracker.debian.org/tracker/CVE-2020-8631">CVE-2020-8631</a></p>

    <p>In cloud-init, relies on Mersenne Twister for a random password, which
    makes it easier for attackers to predict passwords, because rand_str in
    cloudinit/util.py calls the random.choice function.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8632">CVE-2020-8632</a>

    <p>In cloud-init, rand_user_password in cloudinit/config/cc_set_passwords.py
    has a small default pwlen value, which makes it easier for attackers to
    guess passwords.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.7.6~bzr976-2+deb8u1.</p>

<p>We recommend that you upgrade your cloud-init packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2113.data"
# $Id: $
