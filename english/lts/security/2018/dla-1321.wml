<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Jesse Schwartzentruber discovered a use-after-free vulnerability
in Firefox, which could be exploited to trigger an application
crash or arbitrary code execution.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
52.7.3esr-1~deb7u1.</p>

<p>We recommend that you upgrade your firefox-esr packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1321.data"
# $Id: $
