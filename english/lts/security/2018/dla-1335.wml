<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two security vulnerabilities were discovered in the Z shell.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1071">CVE-2018-1071</a>

  <p>Stack-based buffer overflow in the exec.c:hashcmd() function.
  A local attacker could exploit this to cause a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1083">CVE-2018-1083</a>

  <p>Buffer overflow in the shell autocomplete functionality. A local
  unprivileged user can create a specially crafted directory path which
  leads to code execution in the context of the user who tries to use
  autocomplete to traverse the before mentioned path. If the user
  affected is privileged, this leads to privilege escalation.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4.3.17-1+deb7u2.</p>

<p>We recommend that you upgrade your zsh packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1335.data"
# $Id: $
