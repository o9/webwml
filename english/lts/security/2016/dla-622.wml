<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Dawid Golunski from legalhackers.com discovered that Debian's version
of Tomcat 6 was vulnerable to a local privilege escalation. Local
attackers who have gained access to the server in the context of the
tomcat6 user through a vulnerability in a web application were able to
replace the file with a symlink to an arbitrary file.</p>

<p>The full advisory can be found at</p>

<p><url "http://legalhackers.com/advisories/Tomcat-Debian-based-Root-Privilege-Escalation-Exploit.txt"></p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
6.0.45+dfsg-1~deb7u2.</p>

<p>We recommend that you upgrade your tomcat6 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-622.data"
# $Id: $
