<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The code did not check the integer didn't overflow before trying to
resize a buffer. A specially crafted file could result in using memory
past the end of the allocated buffer.</p>

<p>This security CVEs for this issue (<a href="https://security-tracker.debian.org/tracker/CVE-2016-4563">CVE-2016-4563</a>) along with
<a href="https://security-tracker.debian.org/tracker/CVE-2016-4562">CVE-2016-4562</a> and <a href="https://security-tracker.debian.org/tracker/CVE-2016-4564">CVE-2016-4564</a> were based on a security advisory that
will not be made public. This security fix was generated based only on
the surface-level code-change information that is public in GitHub. For
more information on this, see: <a href="http://seclists.org/oss-sec/2016/q2/476">http://seclists.org/oss-sec/2016/q2/476</a></p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
8:6.7.7.10-5+deb7u7.</p>

<p>We recommend that you upgrade your imagemagick packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-517.data"
# $Id: $
