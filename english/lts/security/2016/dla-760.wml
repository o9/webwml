<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Multiple reflected cross-site scripting (XSS) vulnerabilities have been
discovered in SPIP, a website publishing engine written in PHP.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9997">CVE-2016-9997</a>

    <p>It was discovered that the <q>id</q> parameter to the puce_statut action
    isn't sanitized properly. An attacker could inject arbitrary HTML
    code by tricking an authenticated SPIP user to open a specially
    crafted URL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9998">CVE-2016-9998</a>

    <p>It was discovered that the <q>plugin</q> parameter to the info_plugin
    action isn't sanitized properly. An attacker could inject arbitrary
    HTML code by tricking an authenticated SPIP user to open a specially
    crafted URL.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.1.17-1+deb7u8.</p>

<p>We recommend that you upgrade your spip packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-760.data"
# $Id: $
