<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Two issues have been found in libspf2, a library for validating mail
senders with SPF.
Both issues are related to heap-based buffer overflows.</p>


<p>For Debian 9 stretch, these problems have been fixed in version
1.2.10-7+deb9u2.</p>

<p>We recommend that you upgrade your libspf2 packages.</p>

<p>For the detailed security status of libspf2 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libspf2">https://security-tracker.debian.org/tracker/libspf2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2890.data"
# $Id: $
