<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there were two vulnerabilities in libvirt, an
abstraction API for various virtualisation mechanisms:</>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10161">CVE-2019-10161</a>

  <p>Readonly clients could use the API to specify an arbitrary path which
  would be accessed with the permissions of the libvirtd process. An attacker
  with access to the libvirtd socket could use this to probe the existence of
  arbitrary files, cause a denial of service or otherwise cause libvirtd to
  execute arbitrary programs.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10167">CVE-2019-10167</a>

  <p>An arbitrary code execution vulnerability via the API where a
  user-specified binary used to probe the domain's capabilities. Read-only
  clients could specify an arbitrary path for this argument, causing libvirtd
  to execute a crafted executable with its own privileges.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.2.9-9+deb8u7.</p>

<p>We recommend that you upgrade your libvirt packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1832.data"
# $Id: $
