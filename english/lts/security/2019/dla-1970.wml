<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Emil Lerner, beched and d90pwn found a buffer underflow in php5-fpm, a
Fast Process Manager for the PHP language, which can lead to remote
code execution.</p>

<p>Instances are vulnerable depending on the web server configuration, in
particular PATH_INFO handling.  For a full list of preconditions,
check: <a href="https://github.com/neex/phuip-fpizdam">https://github.com/neex/phuip-fpizdam</a></p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
5.6.40+dfsg-0+deb8u7.</p>

<p>We recommend that you upgrade your php5 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1970.data"
# $Id: $
