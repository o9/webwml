<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Two vulnerabilities have been discovered in pdns, an authoritative DNS
server which may result in denial of service via malformed zone records
and excessive NOTIFY packets in a master/slave setup.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10162">CVE-2019-10162</a>

    <p>An issue has been found in PowerDNS Authoritative Server allowing
    an authorized user to cause the server to exit by inserting a
    crafted record in a MASTER type zone under their control. The issue
    is due to the fact that the Authoritative Server will exit when it
    runs into a parsing error while looking up the NS/A/AAAA records it
    is about to use for an outgoing notify.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10163">CVE-2019-10163</a>

    <p>An issue has been found in PowerDNS Authoritative Server allowing
    a remote, authorized master server to cause a high CPU load or even
    prevent any further updates to any slave zone by sending a large
    number of NOTIFY messages. Note that only servers configured as
    slaves are affected by this issue.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.4.1-4+deb8u10.</p>

<p>We recommend that you upgrade your pdns packages.</p>

<p>For the detailed security status of pdns please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/pdns">https://security-tracker.debian.org/tracker/pdns</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1843.data"
# $Id: $
