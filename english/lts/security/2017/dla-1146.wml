<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>mosquitto's persistence file (mosquitto.db) was created in a
world-readable way thus allowing local users to obtain sensitive MQTT
topic information.  While the application has been fixed to set
proper permissions by default, you still have to manually fix
the permissions on any existing file.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.15-2+deb7u2.</p>

<p>We recommend that you upgrade your mosquitto packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1146.data"
# $Id: $
