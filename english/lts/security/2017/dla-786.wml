<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that there was an integer overflow vulnerability
in botan, a cryptography library. This could occur while parsing
untrusted inputs such as X.509 certificates.</p>

<p>For Debian 7 <q>Wheezy</q>, this problem has been fixed in version
1.10.5-1+deb7u2.</p>

<p>We recommend that you upgrade your botan1.10 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-786.data"
# $Id: $
