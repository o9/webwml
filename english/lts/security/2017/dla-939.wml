<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in qemu-kvm, a full
virtualization solution on x86 hardware based on Quick
Emulator(Qemu). The Common Vulnerabilities and Exposures project
identifies the following problems:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9603">CVE-2016-9603</a>

    <p>qemu-kvm built with the Cirrus CLGD 54xx VGA Emulator and the VNC
    display driver support is vulnerable to a heap buffer overflow
    issue.  It could occur when Vnc client attempts to update its
    display after a vga operation is performed by a guest.</p>

    <p>A privileged user/process inside guest could use this flaw to crash
    the Qemu process resulting in DoS OR potentially leverage it to
    execute arbitrary code on the host with privileges of the Qemu
    process.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7718">CVE-2017-7718</a>

     <p>qemu-kvm built with the Cirrus CLGD 54xx VGA Emulator support is
     vulnerable to an out-of-bounds access issue. It could occur while
     copying VGA data via bitblt functions cirrus_bitblt_rop_fwd_transp_
     and/or cirrus_bitblt_rop_fwd_.</p>

     <p>A privileged user inside guest could use this flaw to crash the
     Qemu process resulting in DoS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7980">CVE-2017-7980</a>

     <p>qemu-kvm built with the Cirrus CLGD 54xx VGA Emulator support is
     vulnerable to an out-of-bounds r/w access issues. It could occur
     while copying VGA data via various bitblt functions.</p>

     <p>A privileged user inside guest could use this flaw to crash the
     Qemu process resulting in DoS OR potentially execute arbitrary code
     on a host with privileges of Qemu process on the host.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.1.2+dfsg-6+deb7u21.</p>

<p>We recommend that you upgrade your qemu-kvm packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-939.data"
# $Id: $
