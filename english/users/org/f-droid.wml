# From: "Hans-Christoph Steiner" <hans@eds.org>

<define-tag pagetitle>F-Droid - the definitive source for free software Android apps</define-tag>
<define-tag webpage>https://f-droid.org/</define-tag>

#use wml::debian::users

<p>
    F-Droid was built on Debian GNU/Linux from our beginnings in 2011.
    Debian powers almost all servers used in the whole process of
    inspecting, building, publishing, and hosting apps.  F-Droid is
    strongly aligned with the Debian community in both goals and
    spirit, and many F-Droid core contributors have been inspired by
    Debian.  A couple F-Droid core contributors are even Debian
    Developers.
</p>

<p>Here are some examples of where we use Debian:</p>
<ul>
<li>The buildserver VM instances and CI containers.</li>
<li>The buildserver host machine.</li>
<li>The server that indexes all the files and pushes releases to the web.</li>
<li>The webserver for <tt>f-droid.org</tt></li>
<li>The front-end caching webservers.</li>
<li>Development and CI servers.</li>
<li>Our forum server.</li>
<li>Our Matrix server.</li>
<li>Our verification rebuilders.</li>
<li>Contributors' personal computers.</li>
</ul>
