#use wml::debian::template title="Ευχαριστούμε που κατεβάζετε το Debian!" 
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="1d8e9363ec81c198712c4ba45ff77dd67145b026" maintainer="galaxico"

{#meta#:
<meta http-equiv="refresh" content="3;url=<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">
:#meta#}

<p>Αυτό είναι το Debian <:=substr '<current_initial_release>', 0, 2:>, με το κωδικό όνομα <em><current_release_name></em>, netinst, για την αρχιτεκτονική <: print $arches{'amd64'}; :>.</p>

<p>Αν η μεταφόρτωσή σας δεν ξεκινήσει αυτόματα, πατήστε τον σύνδεσμο <a href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso</a>.</p>
<p>Κατεβάστε το checksum: <a href="<stable-images-url/>/amd64/iso-cd/SHA512SUMS">SHA512SUMS</a> <a href="<stable-images-url/>/amd64/iso-cd/SHA512SUMS.sign">Signature</a></p>



<div class="tip">
	<p><strong>Σημαντικό</strong>: Βεβαιωθείτε να <a href="$(HOME)/CD/verify">επαληθεύσετε την μεταφόρτωσή σας με το checksum</a>.</p>
</div>

<p>Τα ISO του εγκαταστάτη του Debian είναι υβριδικές εικόνες, που σημαίνει ότι μπορούν να εγγραφούν απευθείας σε μέσα CD/DVD/BD ή σε <a href="https://www.debian.org/CD/faq/#write-usb">κλειδιά USB</a>.</p>

<h2 id="h2-1">
	Άλλοι εγκαταστάτες</h2>

<p>Άλλοι εγκαταστάτες και εικόνες, όπως live συστήματα, εγκαταστάτες εκτός σύνδεσης για συστήματα χωρίς δικτυακή σύνδεση, εγκαταστάτες για άλλες
αρχιτεκτονικές CPU, ή περιπτώσεις νέφους, μπορούν να βρεθούν στη σελίδα <a href="$(HOME)/distrib/">Αποκτώντας το Debian</a>.</p>

<p>Ανεπίσημοι εγκαταστάτες με <a href="https://wiki.debian.org/Firmware"><strong>μη ελεύθερο υλισμικό</strong></a>, χρήσιμοι για μερικούς προσαρμογείς δικτύου και εικόνας, μπορούν να μεταφορτωθούν από τη σελίδα <a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/">Ανεπίσημες μη ελεύθερες εικόνες που περιλαμβάνουν πακέτα υλισμικού</a>.</p>

<h2 id="h2-2">Σχετικοί σύνδεσμοι</h2>

<p><a href="$(HOME)/releases/<current_release_name>/installmanual">Οδηγός Εγκατάστασης</a></p>
<p><a href="$(HOME)/releases/<current_release_name>/releasenotes">Σημειώσεις της Έκδοσης</a></p>
<p><a href="$(HOME)/CD/verify">Οδηγός Επαλήθευσης ISO</a></p>
<p><a href="$(HOME)/CD/http-ftp/#mirrors">Εναλλακτικοί ιστότοποι μεταφόρτωσης</a></p>
<p><a href="$(HOME)/releases">Άλλες εκδόσεις</a></p>


