# translation of blends.po to Dutch
# Frans Spiesschaert <Frans.Spiesschaert@yucom.be>, 2017, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: dutch/po/blends.nl.po\n"
"PO-Revision-Date: 2019-02-24 15:54+0100\n"
"Last-Translator: Frans Spiesschaert <Frans.Spiesschaert@yucom.be>\n"
"Language-Team: Debian Dutch l10n Team <debian-l10n-dutch@lists.debian.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Gtranslator 2.91.7\n"

#: ../../english/blends/blend.defs:15
msgid "Metapackages"
msgstr "Metapakketten"

#: ../../english/blends/blend.defs:18
msgid "Downloads"
msgstr "Downloads"

#: ../../english/blends/blend.defs:21
msgid "Derivatives"
msgstr "Derivaten"

#: ../../english/blends/released.data:15
msgid ""
"The goal of Debian Astro is to develop a Debian based operating system that "
"fits the requirements of both professional and hobby astronomers. It "
"integrates a large number of software packages covering telescope control, "
"data reduction, presentation and other fields."
msgstr ""
"Debian Astro stelt zich tot doel een op Debian gebaseerd besturingssysteem "
"te ontwikkelen dat beantwoordt aan de behoeften van zowel professionele als "
"hobby-astronomen. Het project integreert in Debian een groot aantal "
"softwarepakketten die betrekking hebben op telescoopbesturing, datareductie, "
"presentatie en andere gebieden."

#: ../../english/blends/released.data:23
msgid ""
"The goal of DebiChem is to make Debian a good platform for chemists in their "
"day-to-day work."
msgstr ""
"DebiChem stelt zich tot doel met Debian aan chemici een goed platform te "
"bieden voor hun dagelijkse werkzaamheden."

#: ../../english/blends/released.data:31
msgid ""
"The goal of Debian Games is to provide games in Debian from arcade and "
"adventure to simulation and strategy."
msgstr ""
"Debian Games stelt zich tot doel in Debian spellen aan te bieden die gaan "
"van arcade- en avonturenspellen tot simulatie- en strategische spellen."

#: ../../english/blends/released.data:39
msgid ""
"The goal of Debian Edu is to provide a Debian OS system suitable for "
"educational use and in schools."
msgstr ""
"Debian Edu stelt zich tot doel te voorzien in een Debian-systeem dat "
"geschikt is voor educatief gebruik in scholen."

#: ../../english/blends/released.data:47
msgid ""
"The goal of Debian GIS is to develop Debian into the best distribution for "
"Geographical Information System applications and users."
msgstr ""
"Debian GIS stelt zich tot doel om Debian zo te ontwikkelen dat het de beste "
"distributie wordt voor toepassingen voor en gebruikers van een geografisch "
"informatiesysteem."

#: ../../english/blends/released.data:57
msgid ""
"The goal of Debian Junior is to make Debian an OS that children will enjoy "
"using."
msgstr ""
"Debian Junior stelt zich tot doel van Debian een OS te maken dat kinderen "
"leuk vinden om te gebruiken."

#: ../../english/blends/released.data:65
msgid ""
"The goal of Debian Med is a complete free and open system for all tasks in "
"medical care and research. To achieve this goal Debian Med integrates "
"related free and open source software for medical imaging, bioinformatics, "
"clinic IT infrastructure, and others within the Debian OS."
msgstr ""
"Debian Med stelt zich tot doel een volledig vrij en open systeem te maken "
"dat geschikt is voor alle taken op het gebied van medische zorg en "
"onderzoek. Om dat doel te bereiken integreert Debian Med in het OS van "
"Debian vrije en open bronsoftware voor medische beeldvorming, voor bio-"
"informatica, voor klinische IT-infrastructuur, enzovoort."

#: ../../english/blends/released.data:73
msgid ""
"The goal of Debian Multimedia is to make Debian a good platform for audio "
"and multimedia work."
msgstr ""
"Debian Multimedia stelt zich tot doel van Debian een geschikt platform te "
"maken voor audio- en multimedia-werkzaamheden."

#: ../../english/blends/released.data:81
msgid ""
"The goal of Debian Science is to provide a better experience when using "
"Debian to researchers and scientists."
msgstr ""
"Debian Science stelt zich tot doel om onderzoekers en wetenschappers een "
"betere ervaring te bieden bij het gebruik van Debian."

#: ../../english/blends/released.data:89
msgid ""
"The goal of FreedomBox is to develop, design and promote personal servers "
"running free software for private, personal communications. Applications "
"include blogs, wikis, websites, social networks, email, web proxy and a Tor "
"relay on a device that can replace a wireless router so that data stays with "
"the users."
msgstr ""
"FreedomBox stelt zich tot doel om vertrouwelijke servers te ontwerpen, te "
"ontwikkelen en te promoten die op basis van vrije software werken en dienen "
"voor persoonlijke en private communicatie. Tot dit soort toepassingen "
"behoren blogs, wiki's, websites, sociale netwerken, e-mail, web proxy en een "
"Tor-knooppunt op een apparaat dat een draadloze router kan vervangen en "
"waardoor de gebruikers meester blijven over hun gegevens."

#: ../../english/blends/unreleased.data:15
msgid ""
"The goal of Debian Accessibility is to develop Debian into an operating "
"system that is particularly well suited for the requirements of people with "
"disabilities."
msgstr ""
"Debian Accessibility stelt zich tot doel Debian te ontwikkelen tot een "
"besturingssysteem dat goed aangepast is aan de noden van mensen met "
"beperkingen."

#: ../../english/blends/unreleased.data:23
msgid ""
"The goal of Debian Design is to provide applications for designers. This "
"includes graphic design, web design and multimedia design."
msgstr ""
"Debian Design stelt zich tot doel te voorzien in toepassingen voor "
"designers, waaronder grafische designers, webdesigners en "
"multimediadesigners."

#: ../../english/blends/unreleased.data:30
msgid ""
"The goal of Debian EzGo is to provide culture-based open and free technology "
"with native language support and appropriate user friendly, lightweight and "
"fast desktop environment for low powerful/cost hardwares to empower human "
"capacity building and technology development  in many areas and regions, "
"like Africa, Afghanistan, Indonesia, Vietnam  using Debian."
msgstr ""
"Debian EzGo stelt zich tot doel te voorzien in cultuurgebonden open en vrije "
"technologie in de moedertaal van de gebruikers en in een passende "
"gebruikersvriendelijke lichte en snelle desktopomgeving voor "
"kostenefficiënte hardware om Debian te kunnen inzetten bij het versterken "
"van de menselijke capaciteitsontwikkeling en de technologische ontwikkeling "
"in verschillende streken en regio's, zoals Afrika, Afghanistan, Indonesië, "
"Vietnam."

#: ../../english/blends/unreleased.data:38
msgid ""
"The goal of Debian Hamradio is to support the needs of radio amateurs in "
"Debian by providing logging, data mode and packet mode applications and more."
msgstr ""
"Debian Hamradio stelt zich tot doel om in Debian aan de behoeften van "
"radioamateurs te beantwoorden door te voorzien in een logboek en in "
"toepassingen voor gegevensoverdracht in datamodus en pakketmodus, enzovoort."

#: ../../english/blends/unreleased.data:47
msgid ""
"The goal of DebianParl is to provide applications to support the needs of "
"parliamentarians, politicians and their staffers all around the world."
msgstr ""
"DebianParl stelt zich tot doel toepassingen te voorzien die beantwoorden aan "
"de behoeften van parlementariërs, politici en hun stafleden over de hele "
"wereld."

#~ msgid "Debian Accessibility"
#~ msgstr "Debian Accessibility"

#~ msgid "Debian Games"
#~ msgstr "Debian Games"
