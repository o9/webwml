#use wml::debian::template title="Overzetting naar Alpha -- Stand van zaken" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/alpha/menu.inc"
#use wml::debian::translation-check translation="b8114b588961778dbd04974c1464a2f388a90c28"

<h1>Debian GNU/Linux op Alpha &ndash; Stand van zaken</h1>

<p>De overzetting naar Alpha, voor het eerst officieel uitgebracht met Debian
2.1 <q>slink</q>, is nu gearchiveerd: de laatste release met officiële
ondersteuning ervoor was Debian 5.0 <q>lenny</q>.
</p>

<h2>Debian GNU/Linux 5.0 <q>lenny</q></h2>

<p>Dit is de laatste Debian-release met de overzetting naar Alpha.</p>

<h2>Debian GNU/Linux 4.0 <q>etch</q></h2>

<p>Deze release had ondersteuning voor Alpha, maar enkel die machines
welke SRM/aboot konden gebruiken om op te starten, konden met het nieuwe
installatiesysteem geïnstalleerd worden.</p>


<h2>Debian GNU/Linux 3.1 <q>sarge</q></h2>

<p>Deze release had volledige ondersteuning voor Alpha.</p>

<h2>Debian GNU/Linux 3.0 <q>woody</q></h2>

<p>Deze release had solide ondersteuning voor Alpha en is de laatste die
machines ondersteunde die alleen milo gebruiken (d.w.z. niet SRM).</p>

<h2>Debian GNU/Linux 2.2 <q>potato</q></h2>

<p>Potato heeft veel meer software beschikbaar en ondersteunt meer Alphatypes
dan slink.</p>

<h2>Debian GNU/Linux 2.1 <q>slink</q></h2>

<p>De overzetting naar Alpha werd oorspronkelijk uitgebracht met deze release.</p>

<h1>Hardware-ondersteuning</h1>

<p>Vrij veel hardware wordt nu ondersteund op Linux/Alpha. Helaas is de exacte
combinatie van machine, hardware en kernelversie niet triviaal, dus als u uw
combinatie niet kunt vinden in <a href="http://alphalinux.org/ALOHcl/">deze
lijst op alphalinux.org </a>, en als het zoeken in de AXP-lijsten van de
<a href="links#lists">lijstarchieven van Debian en Red Hat</a> ook geen
resultaat oplevert, <a href="https://lists. debian.org/debian-alpha/">teken dan
in</a> op de Debian-Alpha mailinglijst en stel daar uw vraag. Meestal heeft
iemand het al geprobeerd en kan die nuttige informatie geven.</p>
