#use wml::debian::template title="데비안 웹 페이지 번역" BARETITLE=true
#use wml::debian::translation-check translation="8627b3f6f68ecae195c89923923955f8e32ea092" maintainer="kkamagui"
#use wml::fmt::verbatim

<p> 번역자가 쉽게 작업하기 위해 웹 페이지는 여러분이 익숙한 방법과 좀 다르게 만들어집니다.
웹 페이지는 <a href="https://www.shlomifish.org/open-source/projects/website-meta-language/"><tt>wml</tt></a>로 기술된 소스를 이용하여 만들어지며,
언어마다 따로 디렉터리가 있습니다.</p>

<p>데비안 사이트를 완전히 새롭게 번역 시작할 계획이라면,
<a href="#completenew">새 번역 시작하기 섹션</a>을 보세요.</p>

<h2><a name="singlepages">개별 페이지 번역</a></h2>

<p>우리는 WML을 써서 여러 페이지에서 공통적으로 사용되는 특정 요소를 분리합니다.
이는 누군가 HTML 파일 대신 WML 소스 파일을 편집할 필요가 있다는 의미입니다.
현재 소스를 얻으려면
<a href="using_git">Git 사용하기</a>를 보세요.
여러분은 적어도 두 디렉터리를 체크아웃 할 필요가 있습니다:
<tt>webwml/english/</tt> 그리고 <tt>webwml/<var>&lt;language&gt;</var>/</tt> 말이죠.</p> 

<p>한 페이지를 영어에서 여러분의 언어로 번역하려면, 원본 .wml 파일을 번역하고 다른 언어의 디렉터리에 놓아야 합니다.
링크가 동작하려면 상대 경로와 이름이 영어 디렉터리와 같아야 합니다.
</p>

<h3>번역 헤더</h3>
<p>번역자는 헤더의 마지막 <code>#use</code> 문 뒤에 행을 추가해 번역한 원본 파일의 정확한 커밋(commit)을 기록해야 하는데, 
그렇게 하면 <a href="uptodate">업데이트가 쉬워집니다</a>.
그 줄은 다음과 같습니다:
<kbd>#use wml::debian::translation-check translation="<var>&lt;git_commit_hash&gt;</var>"</kbd>
번역할 파일을 <tt>copypage.pl</tt> 도구(강력 권장)를 써서 만들면,
git 커밋 해시가 자동 생성될 겁니다.
<tt>copypage.pl</tt> 사용법은 다음 글에 설명할 겁니다.
</p>

<p>어떤 번역 팀은 이 줄을 각 웹 페이지의 공식 번역자를 표시할 때도 사용합니다.
이렇게 하면 공식 번역자는 영어 페이지가 업데이트되면 자동 메일을 받을 수 있지만,
번역을 업데이트하려면 주의가 필요할 겁니다.
공식 번역자를 표기하기 위해  <code>#use</code>행의 끝에 단순히 여러분의 이름을 관리자로 추가하면 다음과 같습니다:
<kbd>#use wml::debian::translation-check translation="<var>git_commit_hash</var>" maintainer="<var>여러분의 이름</var>"</kbd>.
<tt>copypage.pl</tt>는 여러분이
<tt>DWWW_MAINT</tt> 환경 변수를 설정하거나
<tt>-m</tt> 명령행 스위치를 썼다면 이 행을 자동으로 생성합니다.
</p>

# Remove cvs specific description

<p>웹 페이지 헤더는 webwml 루트 디렉터리의 <tt>copypage.pl</tt> 스크립트를 사용하여 쉽게 만들 수 있습니다.
스크립트는 페이지를 올바른 위치에 복사하고, 필요하면 디렉터리와 makefile을 만들고,
헤더를 자동으로 추가합니다.
복사할 페이지가 저장소에 있다면 경고를 받을 것인데,
이는 페이지가 (너무 오래 되어서) 저장소에서 제거되었거나
누군가 이미 번역을 제출했는데 여러분이 로컬 저장소 사본을 갱신하지 않았기 때문입니다.
</p>

<p><tt>copypage.pl</tt>을 이용해서 번역을 시작하려면 <tt>webwml</tt> root 디렉터리의 <tt>language.conf</tt> 파일을 먼저 설정해야 하는데,
그 파일은 번역할 언어를 결정하는데 쓰일 겁니다.
그 파일은 최대 두 줄이 필요합니다:
첫 번째 줄은 언어명(<tt>korean</tt> 같은)을 써야하고, 두 번째 줄은 선택적으로 유지보수 번역자의 이름을 쓸 수 있습니다.
또한, 여러분은 <tt>DWWW_LANG</tt> 환경변수를 써서 언어명을 설정할 수도 있으며,
<tt>DWWW_MAINT</tt> 환경변수에 여러분의 이름을 넣을 수 있습니다.
이러한 정보는 번역 유지보수자(Maintainer)로서 wml 파일 헤더에 추가될 겁니다.
셋째로 가능한 방법은 명령행에서 언어명과 필요하면 유지보수자를
<tt>-l korean -m "Hong Gildong"</tt>와 같이 사용하고 language.conf 파일을 안 쓰는 방법입니다.
스크립트의 다른 기능들도 있는데, 도움말을 보려면 인자를 전달하지 말고 그냥 실행하면 됩니다.
</p>

<p>여러분이 예를 들어
<kbd>./copypage.pl <var>file</var>.wml</kbd>를
실행했다면, 파일 안에 있는 원본 텍스트를 번역하세요.
파일 안의 주석은 번역하면 안 되는 아이템이 있는지 알려줍니다. 그것을 존중하세요.
형식을 불필요하게 변경하지 마세요. 뭔가 수정할 게 있다면 원본에서 해야 될 겁니다.

<h3>페이지 빌드와 게시</h3>

<p>우리는 <a href="content_negotiation">내용 협상</a>을 쓰므로,
HTML 파일 이름은 <tt><var>file</var>.html</tt>이 아니라
<tt><var>file</var>.<var>&lt;lang&gt;</var>.html</tt>이 되는데, 여기서 <var>&lt;lang&gt;</var>는
두 자리 나라 코드이며, 
<a href="https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes">ISO 639</a>
(e.g. <tt>ko</tt>는 한국어)를 따릅니다.</p>

<p>WML로부터 HTML을 만들려면
<kbd>make <var>file</var>.<var>&lt;lang&gt;</var>.html</kbd>를
실행하세요.
실행했다면 구문이 올바른지 
<kbd>weblint <var>file</var>.<var>&lt;lang&gt;</var>.html</kbd>로
확인하세요.</p>

<p>주의: www-master 서버는 Git에 있는 WML 소스를 이용해서 웹 페이지를 주기적으로 자동 빌드합니다.
이 과정은 대부분 오류가 발생하지 않습니다.
하지만 여러분이 깨진 최상위 번역 파일을 제출했다면, (예를 들어 최상위 파일인 index.wml 같은) 빌드 프로세스를 망가뜨릴 것이며 다른 웹사이트의 업데이트가 정지될 것입니다.
이런 파일들은 주의를 기울여 주세요.</p>

<p>페이지가 잘 준비되었다면, Git에 커밋(Commit) 할 수 있습니다.
이 작업을 직접 할 권한이 있으면, 커밋을 <a href="https://salsa.debian.org/webmaster-team/webwml">webwml
git repository</a>에 push 하세요;
아니면, 그것을 <a href="translation_coordinators">저장소에 쓰기 권한 있는 사람</a>에게 보내세요.
</p>

<h2><a name="completenew">새 번역 시작</a></h2>

<p>데비안 웹 페이지를 새 언어로 번역 시작하려면, 
<a href="mailto:webmaster@debian.org">webmaster@debian.org</a>에
(영어로) 이메일을 보내세요.

<p>무엇보다 먼저, 우리의 소스 트리를 <a href="using_git">Git 사용하기</a>에
설명한 것처럼 바르게 체크아웃 했는지 확인하세요.
</p>

<p>Git을 체크아웃한 후에,
english/ 및 다른 것들과 나란히 여러분이 번역할 최상위 디렉터리를 만드세요.
번역 디렉터리의 이름은 영어이고 전부 소문자(예. "korean", "Korea" 아님)여야 합니다.
</p>

<p><tt>Make.lang</tt> 및 <tt>.wmlrc</tt> 파일을 english/ 디렉터리에서 새 번역 디렉터리로 복사하세요.
이 파일들은 WML 파일들로부터 번역을 빌드 하는데 꼭 필요합니다.
그것들을 새 디렉터리에 복사한 후에, 아래 것들을 바꾸세요:</p>

<ol>
  <li><tt>Make.lang</tt> 파일 안에 있는 변수 LANGUAGE를 바꿉니다.

  <li><tt>.wmlrc</tt> 파일 안에 있는 변수 CUR_LANG, CUR_ISO_LANG 그리고 CHARSET를 바꿉니다.
정렬이 필요하면 CUR_LOCALE를 추가합니다.

  <li>어떤 언어는 charset을 다루기 위해 추가 처리가 필요할 수 있습니다.
이를 위해 wml에 --prolog와 --epilog 옵션을 사용할 수 있습니다.
이를 이용하려면 <tt>Make.lang</tt>에 WMLPROLOG와 WMLEPILOG 변수를 사용합니다.


  <li>변수 LANGUAGES는 최상위 <tt>webwml/Makefile</tt> 파일에서 바꾸어야
여러분의 언어가 다른 언어들과 함께 www.debian.org에서 빌드됩니다.
우리는 여러분이 이러한 특정 변경을 웹마스터에게 맡기기를 바랍니다.
왜냐면 여러분은 VCS에서 여러분의 번역을 체크아웃했을 때 번역이 망가졌다는 것을 모를 수 있고,
망가진 번역 파일은 우리 웹 사이트의 나머지 빌드 과정을 망가뜨릴 수 있기 때문입니다.
</ol>

<p>위의 과정이 끝나면, 아래 줄을 그 디렉터리에 있는 새 파일 "Makefile"에 넣으세요:

<pre>
<protect>include $(subst webwml/<var>yourlanguagedir</var>,webwml/english,$(CURDIR))/Makefile</protect>
</pre>

<p>(<var>yourlanguagedir</var>를 여러분의 언어명 디렉터리로 바꾸세요.)</p>

<p>여러분 언어명 디렉터리에 "po" 디렉터리를 만들고,
같은 Makefile을 그 하위 디렉터리에 복사하세요. (<kbd>cp ../Makefile .</kbd> . )
</p>

<p>po/ 디렉터리에서, <kbd>make init-po</kbd> 명령을 돌려서 초기 .po 파일 세트를 만드세요.</p>

<p>이제 뼈대가 설정되었고, 번역을 템플릿에 사용되는 공용 WML 태그에 추가할 수 있습니다.
번역할 첫 템플릿은 웹 페이지 모두에 나타나는데, 머리말 키워드, 네비게이션 바 엔트리, 꼬리말 같은 겁니다.
</p>

# The page on <a href="using_wml">using WML</a> has more information on this.

<p><code>po/templates.<var>xy</var>.po</code> (xy는 언어의 두 자리 코드, 한국은 ko) 파일에서
번역을 시작하세요.
모든 <code>msgid "<var>something</var>"</code> 다음에 <code>msgstr ""</code>이 있는데
여기서 <var>something</var>의 번역을 <code>msgstr</code> 다음의 따옴표 안에 넣으세요.
</p>

<p>모든.po 파일의 모든 문자열을 번역할 필요는 없으며, 현재 번역된 페이지에서 실제 필요한 것만 하세요.
문자열을 번역해야 되는지 보려면, .po 파일에서 각 <code>msgid</code> 문의 바로 위에 있는 코멘트를 보세요.
참조된 파일이 <tt>english/template/debian</tt>에 있으면, 대개 번역해야 할 겁니다.
아니라면, 미루어 두었다가 필요한 웹페이지의 관련 섹션을 번역할 때 하면 됩니다.
</p>

<p>po/ 파일의 핵심은 번역자에게 일을 쉽게 만드는 것이므로,
번역자들이 <tt>english/template/debian</tt> 디렉터리 안에 있는 (거의)어떤 것도 편집할 필요 없습니다.
무언가 템플릿 디렉터리에서 설정된 방법이 잘못된 것을 발견하면
문제를 일반적인 방법으로 수정할 수 있는지 확인하고(다른 사람에게 자유롭게 물어보고 도와줄 수 있는지 알아보세요),
템플릿 안에 실제 번역을 커밋하지 마세요(대개 큰 문제가 될 수 있음).
</p>

<p>올바르게 하고 있는지 확신이 들지 않으면, debian-www 메일링 리스트에 물어보고 커밋하세요.
</p>

<p>주의: 변경한 필요가 있는 것을 발견하면, 무엇을 왜 바꾸었는지를
debian-www로 보내서 문제를 수정할 수 있게 하세요.

<p>템플릿 뼈대가 되면, 프론트 페이지와 *.wml 파일들 번역을 시작할 수 있습니다.
먼저 번역할 파일 목록은 <a href="translation_hints">힌트 페이지</a>를 보세요.
<a href="#singlepages">이 페이지의 맨 위</a>에 설명된 *.wml 페이지들을 번역하세요.
</p>

<h3>오래된 번역 되살리기</h3>

<p><a href="uptodate">어떻게 최신 번역을 유지할까</a>에 설명된 것처럼, 웹사이트의 오래된 번역은 
오래도록 업데이트 없이 시간이 오래 지나면 자동으로 사라질 수 있습니다.</p>
<p>어떤 파일이 과거 언젠가 사라졌고 당신이 그 파일을 더 편집해서 체크아웃하고 싶으면,
커밋 히스토리를 Git의 표준 명령을 통해 찾을 수 있습니다.</p>

<p>예를 들어, 지워진 파일이 "deleted.wml"이면, 아래 명령어를 실행해서 히스토리를 찾을 수 있을 겁니다:</p>

<verbatim>
   git log --all --full-history -- <path/to/file/deleted.wml>
</verbatim>

<p>여러분이 지워진 파일의 정확한 커밋을 찾으려면,
커밋의 해시 문자열을 갖고 찾아야 합니다.
이 커밋의 변경 내용과 관련된 자세한 정보를 보려면
<code>git show</code> 하위 명령을 쓰세요:</p>

<verbatim>
  git show <COMMIT_HASH_STRING> -- <path/to/file/deleted.wml>
</verbatim>

<p>커밋이 정확히 지워진 파일에 관한 것이면, <code>git checkout</code>을 써서 파일을 워크스페이스에
되돌릴 수 있습니다:</p>

<verbatim>
  git checkout <COMMIT_HASH_STRING>^ -- <path/to/file/deleted.wml>
</verbatim>

<p>물론, 이렇게 한 다음 문서를 업데이트 하고 다시 체크인 하세요. 아니면 지워질 수 있습니다.

<h3>남은 이야기</h3>

<p>위의 설명은 여러분이 시작하기에 충분할 겁니다.
다음에는, 좀 더 자세한 설명과 추가적인 유용한 정보를 제공하는
아래의 문서를 참고하고 싶을 겁니다.
</p>

<ul>
<li>어떻게 시작할지 아이디어를 줄 몇 <a href="examples">예제</a>가 있습니다.
<li>일반 질문에 대한 답과 도움을 주는 힌트가 <a href="translation_hints">번역 힌트</a> 페이지에 있습니다.
<li><a href="uptodate">번역을 최신으로 유지</a>하는데 도움되는 메커니즘이 있습니다.

<li>여러분의 번역 상태와 다른 번역과의 비교를 어떻게 하는지 보려면 <a href="stats/">통계</a>를 보세요.
</ul>

<p>우리가 한 일이 여러분이 페이지들을 가능한 한 쉽게 번역하는 데 도움이 되길 바랍니다.
이미 언급한 바와 같이, 질문이 있으면, 여러분이 <a
href="mailto:debian-www@lists.debian.org">debian-www</a>메일링 리스트에 물어볼 수 있습니다.
