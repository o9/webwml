#use wml::debian::blend title="Usando os metapacotes"
#use "../navbar.inc"
#use wml::debian::translation-check translation="7699c3698cd2627c6da0718bd3329188b02ad8c6"

<p>Os metapacotes são usados pelo blend como uma maneira conveniente de reunir
pacotes de software relacionados. Cada metapacote, quando instalado, fará com
que o sistema de administração de pacotes instale os pacotes relativos
à tarefa.</p>

<p>Os seguintes metapacotes são mantidos atualmente pelo blend:</p>

<table>
	<tr><th>Nome da tarefa</th><th>Metapacote</th><th>Descrição</th><th>Catálogo</th></tr>
	<tr>
		<td>gis-data</td>
		<td><a href="https://packages.debian.org/unstable/gis-data"><code>gis-data</code></a></td>
		<td>Esta tarefa contém pacotes que fornecem dados que podem ser
                    usados por diferentes aplicações GIS.</td>
		<td><a href="https://blends.debian.org/gis/tasks/data">Link</a></td>
	</tr>
	<tr>
		<td>gis-devel</td>
		<td><a href="https://packages.debian.org/unstable/gis-devel"><code>gis-devel</code></a></td>
		<td>Esta tarefa configura seu sistema para desenvolvimento GIS.</td>
		<td><a href="https://blends.debian.org/gis/tasks/devel">Link</a></td>
	</tr>
	<tr>
		<td>gis-gps</td>
		<td><a href="https://packages.debian.org/unstable/gis-gps"><code>gis-gps</code></a></td>
		<td>Conjunto de pacotes Debian que lidam com dispositivos GPS e dados.</td>
		<td><a href="https://blends.debian.org/gis/tasks/gps">Link</a></td>
	</tr>
	<tr>
		<td>gis-osm</td>
		<td><a href="https://packages.debian.org/unstable/gis-osm"><code>gis-osm</code></a></td>
		<td>Conjunto de pacotes Debian que lidam com dados do OpenStreetMap.</td>
		<td><a href="https://blends.debian.org/gis/tasks/osm">Link</a></td>
	</tr>
	<tr>
		<td>gis-remotesensing</td>
		<td><a href="https://packages.debian.org/unstable/gis-remotesensing"><code>gis-remotesensing</code></a></td>
		<td>Pacotes Debian que lidam com sensoriamento remoto (por
                    exemplo, Synthetic Aperture Radar -- SAR), processamento
                    (interferometria, polarimetria, visualização de dados, etc.)
                    e observação da Terra.</td>
		<td><a href="https://blends.debian.org/gis/tasks/remotesensing">Link</a></td>
	</tr>
	<tr>
		<td>gis-statistics</td>
		<td><a href="https://packages.debian.org/unstable/gis-statistics"><code>gis-statistics</code></a></td>
		<td>Conjunto de pacotes Debian que são úteis para fazer
		    estatísticas com dados geográficos.</td>
		<td><a href="https://blends.debian.org/gis/tasks/statistics">Link</a></td>
	</tr>
	<tr>
		<td>gis-web</td>
		<td><a href="https://packages.debian.org/unstable/gis-web"><code>gis-web</code></a></td>
		<td>Pacotes Debian que lidam com informação geográfica para ser
		    apresentada na web, nos chamados servidores de mapa de bloco
		    (map tile servers). Eles são muito úteis ao tentar
		    configurar o servidor de blocos do OpenStreetMap, mas não se
		    restringem somente aos dados do OpenStreetMap.</td>
		<td><a href="https://blends.debian.org/gis/tasks/web">Link</a></td>
	</tr>
	<tr>
		<td>gis-workstation</td>
		<td><a href="https://packages.debian.org/unstable/gis-workstation"><code>gis-workstation</code></a></td>
		<td>Esta tarefa configura o sistema para ser uma estação de
		    trabalho GIS para processar informação geográfica e para
		    fazer mapas.</td>
		<td><a href="https://blends.debian.org/gis/tasks/workstation">Link</a></td>
	</tr>
</table>

<p>Para instalar qualquer um dos metapacotes de tarefas, use sua ferramenta
preferida de administração de pacotes como você faria com qualquer outro
pacote Debian. Para
<code>apt-get</code>:</p>

<pre>apt-get install gis-&lt;task&gt;</pre>

<p>Se você quer instalar o blend inteiro:</p>

<pre>apt-get install gis-data gis-devel gis-gps gis-osm gis-remotesensing gis-statistics gis-web gis-workstation</pre>
