<define-tag pagetitle>Atualização Debian 9: 9.12 lançado</define-tag>
<define-tag release_date>2020-02-08</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="2913230d58de12a2d0daeab2fd1f532a65fa5c2a"

<define-tag release>9</define-tag>
<define-tag codename>stretch</define-tag>
<define-tag revision>9.12</define-tag>


<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>O projeto Debian tem o prazer de anunciar a décima segunda atualização de sua
versão antiga (oldstable) do Debian <release> (codinome <q><codename></q>).
Esta versão pontual adiciona principalmente correções para problemas de
segurança, junto com alguns ajustes para problemas sérios. Avisos de segurança
já foram publicados em separado e são referenciados quando disponíveis.</p>

<p>Por favor note que a versão pontual não constitui uma nova versão do Debian
<release>, mas apenas atualiza alguns dos pacotes já incluídos. Não há
necessidade de se desfazer das antigas mídias do <q><codename></q>. Após a
instalação, os pacotes podem ser atualizados para as versões atuais usando um
espelho atualizado do Debian.</p>

<p>As pessoas que frequentemente instalam atualizações de security.debian.org
não terão que atualizar muitos pacotes, e a maioria dessas atualizações estão
incluídas na versão pontual.</p>

<p>Novas imagens de instalação logo estarão disponíveis nos locais
habituais.</p>

<p>A atualização de uma instalação existente para esta revisão pode ser feita
ao apontar o sistema de gerenciamento de pacotes para um dos muitos espelhos
HTTP do Debian. Uma lista abrangente de espelhos está disponível em:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Correções de bugs gerais</h2>

<p>Esta atualização da versão antiga (oldstable) adiciona algumas correções
importantes para os seguintes pacotes:</p>

<table border=0>
<tr><th>Pacote</th>               <th>Justificativa</th></tr>
<correction base-files "Atualização para nova versão pontual">
<correction cargo "Nova versão upstream para suporte ao Firefox ESR backports; Correção de bootstrap para armhf">
<correction clamav "Nova versão upstream; correção de problemas com negação de serviço [CVE-2019-15961]; remove opção ScanOnAccess, trocada com clamonacc">
<correction cups "Corrige validação de linguagem padrão em ippSetValuetag [CVE-2019-2228]">
<correction debian-installer "Reconstruído com os mais recentes oldstable-proposed-updates; configurado gfxpayload=keep nos submenus para corrigir fontes ilegíveis em monitores hidpi em imagens netboot inicializadas com EFI; Atualizado padrão USE_UDEBS_FROM default de instável (unstable) para stretch, para ajudar os usuários a realizar construções (builds) locais">
<correction debian-installer-netboot-images "Reconstruído com os mais recentes stretch-proposed-updates">
<correction debian-security-support "Atualizado status de suporte de segurança de diversos pacotes">
<correction dehydrated "Nova versão upstream; us da API ACMEv2 API por padrão">
<correction dispmua "Nova versão upstream compatível com Thunderbird 68">
<correction dpdk "Nova versão upstream estável (stable); correçao de regressão vhost introduzida pelo reparo para o CVE-2019-14818">
<correction fence-agents "Corrige remoção incompleta de fence_amt_ws">
<correction fig2dev "Permite sequências de caracteres Fig v2 terminando com múltiplos ^A [CVE-2019-19555]">
<correction flightcrew "Correções de segurança [CVE-2019-13032 CVE-2019-13241]">
<correction freetype "Trata corretamente delta em fontes TrueType GX, corrigindo a renderização de fontes de sugestão variável no Chromium e Firefox">
<correction glib2.0 "Assegurar que clientes libdbus possam autenticar com um GDBusServer da mesma forma que com o ibus">
<correction gnustep-base "Correção de vulnerabilidade de amplificação">
<correction italc "Correções de segurança [CVE-2018-15126 CVE-2018-15127 CVE-2018-20019 CVE-2018-20020 CVE-2018-20021 CVE-2018-20022 CVE-2018-20023 CVE-2018-20024 CVE-2018-20748 CVE-2018-20749 CVE-2018-20750 CVE-2018-6307 CVE-2018-7225 CVE-2019-15681]">
<correction libdate-holidays-de-perl "Marca o dia internacional das crianças (20 de setembro) como feriado na Turíngia a partir de 2019">
<correction libdatetime-timezone-perl "Atualizada dados inclusos">
<correction libidn "Corrige vulnerabilidade de negação de serviço no tratamento de Punycode [CVE-2017-14062]">
<correction libjaxen-java "Corrige falha de construção permitindo testes de falhas">
<correction libofx "Corrige problema de perda de referência de ponteiro  nulo [CVE-2019-9656]">
<correction libole-storage-lite-perl "Corrige a interpretação de anos a partir de 2020">
<correction libparse-win32registry-perl "corrige a interpretação de anos a partir de 2020">
<correction libperl4-corelibs-perl "corrige a interpretação de anos a partir de 2020">
<correction libpst "Corrige detecção de get_current_dir_name and e retorno truncado">
<correction libsixel "Corrige diversas questões de segurança [CVE-2018-19756 CVE-2018-19757 CVE-2018-19759 CVE-2018-19761 CVE-2018-19762 CVE-2018-19763 CVE-2019-3573 CVE-2019-3574]">
<correction libsolv "Corrige estouro do buffer de pilha [CVE-2019-20387]">
<correction libtest-mocktime-perl "Corrige a interpretação de anos a partir de 2020">
<correction libtimedate-perl "Corrige a interpretação de anos a partir de 2020">
<correction libvncserver "RFBserver: Não vaze pilha de memória para o remoto  [CVE-2019-15681]; resolve congelamento durante o encerramento da conexão e uma falha de segmentação em servidores VNC multitarefa; corrige problema de conexão a servidores VMWare; Corrige falha de x11vnc quando o vncviewer se conecta">
<correction libxslt "Corrige ponteiro oscilante em xsltCopyText [CVE-2019-18197]">
<correction limnoria "Corrige divulgação remota de informações e possível execução de código remoto na extensão Math [CVE-2019-19010]">
<correction linux "Nova versão estável upstream">
<correction linux-latest "Atualização para o kernel Linux ABI 4.9.0-12">
<correction llvm-toolchain-7 "Desabilita conector dourado de s390x; bootstrap com -fno-addrsig, binutils do stretch não funciona com mips64el">
<correction mariadb-10.1 "Nova versão estável upstream [CVE-2019-2974 CVE-2020-2574]">
<correction monit "Implementa valor de cookie CSRF independente de posição">
<correction node-fstream "Corta um link se estiver no caminho de um arquivo [CVE-2019-13173]">
<correction node-mixin-deep "Corrige poluição protótipo [CVE-2018-3719 CVE-2019-10746]">
<correction nodejs-mozilla "Novo pacote de suporte a portes Firefox ESR de versões mais novas">
<correction nvidia-graphics-drivers-legacy-340xx "Nova versão estável upstream">
<correction nyancat "Reconstruído em um ambiente limpo para adicionar unidade systemd para o nyancat-server">
<correction openjpeg2 "corrige estouro de pilha [CVE-2018-21010], estouro de inteiros [CVE-2018-20847] e divisão por zero [CVE-2016-9112]">
<correction perl "Corrige a interpretação de anos a partir de 2020">
<correction php-horde "Corrige problema de cross-site scripting armazenado no Horde Cloud Block [CVE-2019-12095]">
<correction postfix "Nova versão upstream estável; correção de baixa performance no TCP loopback">
<correction postgresql-9.6 "Nova versão upstream">
<correction proftpd-dfsg "Corrige referência de ponteiro nulo em verificações CRL [CVE-2019-19269]">
<correction pykaraoke "Corrige caminho para fontes">
<correction python-acme "Troca para protocolo POST-as-GET">
<correction python-cryptography "Corrige falhas no conjunto de testes quando construído de acordo com versões mais recentes do OpenSSL">
<correction python-flask-rdf "Corrige dependências ausentes em python3-flask-rdf">
<correction python-pgmagick "Trata a detecção da versão de atualização de segurança da graphicsmagick que se identificam como versão 1.4">
<correction python-werkzeug "Assegura que contêineres Docker tenham PINs de depuração únicos  [CVE-2019-14806]">
<correction ros-ros-comm "Corrige problema de estouro de buffer [CVE-2019-13566]; corrige estouro de inteiros [CVE-2019-13445]">
<correction ruby-encryptor "Ignora falhas de testes, corrige falhas de construção">
<correction rust-cbindgen "Novo pacote de suporte a retroportes do Firefox ESR">
<correction rustc "Nova versão upstream, para apoiar o Firefox ESR backports">
<correction safe-rm "Previne a instalação em (e assim quebrá-los) ambientes /usr fundidos">
<correction sorl-thumbnail "Solução de contorno para uma exceção pgmagick">
<correction sssd "sysdb: limpa a entrada do filtro de busca [CVE-2017-12173]">
<correction tigervnc "Atualizações de segurança [CVE-2019-15691 CVE-2019-15692 CVE-2019-15693 CVE-2019-15694 CVE-2019-15695]">
<correction tightvnc "Correções de segurança [CVE-2014-6053 CVE-2018-20021 CVE-2018-20022 CVE-2018-20748 CVE-2018-7225 CVE-2019-8287 CVE-2019-15678 CVE-2019-15679 CVE-2019-15680 CVE-2019-15681]">
<correction tmpreaper "Adição de <q>--protect '/tmp/systemd-private*/*'</q> ao agendamento cron para prevenir quebra nos serviços systemd que tenham  PrivateTmp=true">
<correction tzdata "Nova versão upstream">
<correction ublock-origin "Nova versão upstream, compatível com Firefox ESR68">
<correction unhide "Corrige esgotamento de pilha">
<correction x2goclient "Exclui ~/, ~user{,/}, ${HOME}{,/} e $HOME{,/} dos caminhos de destino em modo scp; corrige regressão com novas versões libssh com correções para CVE-2019-14889 aplicadas">
<correction xml-security-c "Corrige <q>DSA verification crashes OpenSSL on invalid combinations of key content</q>">
</table>


<h2>Atualizações de segurança</h2>


<p>Esta revisão adiciona as seguintes atualizações de segurança para a versão
antiga (oldstable).
A equipe de Segurança já lançou um aviso para cada uma dessas
atualizações:</p>

<table border=0>
<tr><th>ID do aviso</th>  <th>Pacote</th></tr>
<dsa 2019 4474 firefox-esr>
<dsa 2019 4479 firefox-esr>
<dsa 2019 4509 apache2>
<dsa 2019 4509 subversion>
<dsa 2019 4511 nghttp2>
<dsa 2019 4516 firefox-esr>
<dsa 2019 4517 exim4>
<dsa 2019 4518 ghostscript>
<dsa 2019 4519 libreoffice>
<dsa 2019 4522 faad2>
<dsa 2019 4523 thunderbird>
<dsa 2019 4525 ibus>
<dsa 2019 4526 opendmarc>
<dsa 2019 4528 bird>
<dsa 2019 4529 php7.0>
<dsa 2019 4530 expat>
<dsa 2019 4531 linux>
<dsa 2019 4532 spip>
<dsa 2019 4535 e2fsprogs>
<dsa 2019 4537 file-roller>
<dsa XXXX 4539 openssl>
<dsa 2019 4540 openssl1.0>
<dsa 2019 4541 libapreq2>
<dsa 2019 4542 jackson-databind>
<dsa 2019 4543 sudo>
<dsa 2019 4545 mediawiki>
<dsa 2019 4547 tcpdump>
<dsa 2019 4548 openjdk-8>
<dsa XXXX 4549 firefox-esr>
<dsa 2019 4550 file>
<dsa 2019 4552 php7.0>
<dsa 2019 4554 ruby-loofah>
<dsa 2019 4555 pam-python>
<dsa 2019 4557 libarchive>
<dsa 2019 4559 proftpd-dfsg>
<dsa 2019 4560 simplesamlphp>
<dsa 2019 4564 linux>
<dsa 2019 4565 intel-microcode>
<dsa 2019 4567 dpdk>
<dsa 2019 4568 postgresql-common>
<dsa 2019 4569 ghostscript>
<dsa 2019 4571 thunderbird>
<dsa 2019 4573 symfony>
<dsa 2019 4574 redmine>
<dsa 2019 4576 php-imagick>
<dsa 2019 4578 libvpx>
<dsa 2019 4580 firefox-esr>
<dsa 2019 4581 git>
<dsa 2019 4582 davical>
<dsa 2019 4584 spamassassin>
<dsa 2019 4585 thunderbird>
<dsa 2019 4587 ruby2.3>
<dsa 2019 4588 python-ecdsa>
<dsa 2019 4589 debian-edu-config>
<dsa 2019 4590 cyrus-imapd>
<dsa 2019 4591 cyrus-sasl2>
<dsa 2019 4592 mediawiki>
<dsa 2019 4593 freeimage>
<dsa 2019 4594 openssl1.0>
<dsa 2019 4595 debian-lan-config>
<dsa 2019 4596 tomcat8>
<dsa XXXX 4596 tomcat-native>
<dsa 2020 4597 netty>
<dsa 2020 4598 python-django>
<dsa 2020 4600 firefox-esr>
<dsa 2020 4601 ldm>
<dsa 2020 4602 xen>
<dsa 2020 4603 thunderbird>
<dsa 2020 4604 cacti>
<dsa 2020 4607 openconnect>
<dsa 2020 4609 python-apt>
<dsa XXXX 4611 opensmtpd>
<dsa 2020 4612 prosody-modules>
<dsa 2020 4614 sudo>
<dsa 2020 4615 spamassassin>
</table>

<h2>Pacotes removidos</h2>

<p>Os seguintes pacotes foram removidos devido circunstâncias além do nosso
controle:</p>

<table border=0>
<tr><th>Pacote</th>               <th>Justificativa</th></tr>
<correction firetray "Incompatibilidade com as versões atuais do Thunderbird">
<correction koji "Questões de segurança">
<correction python-lamson "Quebrado por mudanças no python-daemon">
<correction radare2 "Questões de segurança; versão upstream não oferece suporte estável">
<correction ruby-simple-form "Não utilizado; questões de segurança">
<correction trafficserver "Não suportado">

</table>

<h2>Instalador do Debian</h2>
<p>O instalador foi atualizado para incluir as correções incorporadas
na versão antiga (oldstable) pela versão pontual.</p>

<h2>URLs</h2>

<p>As listas completas dos pacotes que foram alterados por esta revisão:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>A atual versão antiga (oldstable):</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Atualizações propostas (proposed updates) para a versão antiga (oldstable):</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>informações da versão antiga (oldstable) (notas de lançamento, errata, etc):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Anúncios de segurança e informações:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Sobre o Debian</h2>

<p>O projeto Debian é uma associação de desenvolvedores(as) de Software Livre
que dedicam seu tempo e esforço como voluntários(as) para produzir o sistema
operacional livre Debian.</p>

<h2>Informações de contato</h2>

<p>Para mais informações, por favor visite as páginas web do Debian em
<a href="$(HOME)/">https://www.debian.org/</a>, envie um e-mail (em inglês) para
&lt;press@debian.org&gt;, ou entre em contato (em inglês) com a equipe de
lançamento da antiga (oldstable) em &lt;debian-release@lists.debian.org&gt;.</p>
