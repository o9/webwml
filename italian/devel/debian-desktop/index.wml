#use wml::debian::template title="Debian sul Desktop"  MAINPAGE="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="5946007f07ed802a4ea96fe8dfa5ef2681f41b18" maintainer="Luca Monducci"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#philosophy">La nostra filosofia</a></li>
<li><a href="#help">Come potete aiutarci</a></li>
<li><a href="#join">Partecipa anche tu</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debian Desktop è un
gruppo di volontari che vogliono creare il miglior sistema operativo
possibile per i computer di casa e aziendali. Il nostro motto è <q>Software
che funziona</q>. Il nostro obiettivo è: introdurre Debian, GNU e Linux
nel mondo tradizionale.</p>
</aside>

<h2><a id="philosophy">La nostra filosofia</a></h2>

<p>
Sappiamo che già esistono molti <a
href="https://wiki.debian.org/DesktopEnvironment">Ambienti Desktop</a>,
vogliamo sostenerne l'uso ed essere certi del loro buon funzionamento in
Debian. Facciamo tutto il possibile per rendere le cose facili per i
principianti e allo stesso tempo permettere agli utenti esperti di
modificare le cose a proprio piacimento.
</p>

<p>
Cerchiamo di garantire che i programmi siano configurati per i tipici usi
desktop. Ad esempio, il normale account utente creato nella fase di
installazione dovrebbe avere l'autorizzazione per visionare video,
ascoltare audio, stampare e amministrare il sistema attraverso sudo.
Inoltre cerchiamo di garantire che il numero di domande poste agli utenti
da <a href="https://wiki.debian.org/debconf">debconf</a> (lo strumento di
Debian per gestire la configurazione) sia il più basso possibile. Non è
necessario presentare complicati dettagli tecnici durante l'installazione,
invece le domande di debconf devono essere comprensibili a tutti, un neofita
potrebbe persino non sapere a cosa fanno riferimento e un utente esperto
può comunque configurare al meglio l'ambiente desktop una volta completata
l'installazione.
</p>

<h2><a id="help">Come potete aiutarci</a></h2>

<p>
Stiamo cercando persone motivate che rendano realizzabili le cose. Non c'è
bisogno di essere sviluppatori ufficiali (DD) per iniziare e produrre
pacchetti e patch. Il gruppo centrale Debian Desktop garantisce che il
vostro lavoro sarà integrato. Ecco alcune cose che potete fare:
</p>

<ul>
  <li>
    Testare l'ambiente desktop predefinito (oppure uno qualsiasi degli
	altri) del prossimo rilascio. Installate una delle <a
	href="$(DEVEL)/debian-installer/">immagini di test</a> e inviate le
	vostre osservazioni alla lista di messaggi <a
	href="https://lists.debian.org/debian-desktop/">debian-desktop</a>.
  </li>
  <li>
    Entrare nel <a href="https://wiki.debian.org/DebianInstaller/Team">Debian
	Installer Team</a> e lavorare sul <a
	href="$(DEVEL)/debian-installer/">debian-installer</a>, l'interfaccia
	GTK+ ha bisogno di voi.
  </li>
  <li>
	Contribuire nei gruppi <a
	href="https://wiki.debian.org/Teams/DebianGnome">Debian GNOME</a>,
	<a href="https://qt-kde-team.pages.debian.net/">Debian Qt/KDE e
	Debian KDE Extra</a> o <a
	href="https://salsa.debian.org/xfce-team/">Debian Xfce</a> partecipando
	alla preparazione dei pacchetti, cercando dei bug, lavorando alla
	documentazione, facendo dei test o in altri modi.
  </li>
  <li>
    Contribuire al miglioramento di <a
	href="https://packages.debian.org/debconf">debconf</a> abbassando le
	priorità delle domande, rimuovendo quelle non necessarie dai pacchetti
	o rendendo quelle indispensabili facili da capire.
  </li>
  <li>
    Avete delle capacità grafiche? Perché non lavorare a <a
	href="https://wiki.debian.org/DebianDesktop/Artwork">Debian Desktop
    Artwork</a>.
  </li>
</ul>

<h2><a id="join">Partecipa anche tu</a></h2>

<aside class="light">
  <span class="fa fa-users fa-4x"></span>
</aside>

<ul>
  <li>
    <strong>Wiki:</strong> visita il nostro wiki <a
	href="https://wiki.debian.org/DebianDesktop">DebianDesktop</a> 
	(una parte degli articoli nel wiki non è aggiornata).
  </li>
  <li>
    <strong>Lista di messaggi:</strong> partecipa alle discussioni nella
	lista di messaggi <a
	href="https://lists.debian.org/debian-desktop/">debian-desktop</a>.
  </li>
  <li>
    <strong>Canale IRC:</strong> fai una chat con noi su IRC. Entra
    nel canale #debian-desktop su <a href="http://oftc.net/">OFTC IRC</a>
    (irc.debian.org).
  </li>
</ul>
