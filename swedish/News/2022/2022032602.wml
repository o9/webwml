<define-tag pagetitle>Uppdaterad Debian 10; 10.12 utgiven</define-tag>
<define-tag release_date>2022-03-26</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="9264c23e43e374bf590c29166dde81515943b562"


<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.12</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Debianprojektet presenterar stolt sin tolfte uppdatering till dess
gamla stabila utgåva Debian <release> (med kodnamnet <q><codename></q>). 
Denna punktutgåva lägger huvudsakligen till rättningar för säkerhetsproblem,
tillsammans med ytterligare rättningar för allvarliga problem. Säkerhetsbulletiner
har redan publicerats separat och refereras när de finns tillgängliga.</p>

<p>Vänligen notera att punktutgåvan inte innebär en ny version av Debian 
<release> utan endast uppdaterar några av de inkluderade paketen. Det behövs
inte kastas bort gamla media av <q><codename></q>. Efter installationen
kan paket uppgraderas till de aktuella versionerna genom att använda en uppdaterad
Debianspegling.</p>

<p>De som frekvent installerar uppdateringar från security.debian.org kommer inte att behöva
uppdatera många paket, och de flesta av sådana uppdateringar finns
inkluderade i punktutgåvan.</p>

<p>Nya installationsavbildningar kommer snart att finnas tillgängliga på de vanliga platserna.</p>

<p>En uppgradering av en existerande installation till denna revision kan utföras genom att
peka pakethanteringssystemet på en av Debians många HTTP-speglingar.
En utförlig lista på speglingar finns på:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Skärpning av OpenSSL:s kontroll av signaturalgoritm</h2>

<p>OpenSSL-uppdateringen som tillhandahålls i denna punktutgåva inkluderar
en förändring som säkerställer att den efterfrågade signaturalgoritmen
stöds av den aktiva säkerhetsnivån.</p>

<p>Även om detta inte kommer att påverka de flesta användningsområden,
kan det leda till att felmeddelanden genereras om en icke-stödd
algoritm efterfrågas - exempelvis användning av RSA+SHA1-signaturer med
standardsäkerhetsnivån 2.</p>

<p>I sådana fall måste säkerhetsnivån uttryckligen sänkas, antingen
för individuella förfrågningar eller mer globalt. Detta kan kräva
ändringar till konfigurationen av applikationer. För själva OpenSSL kan
en sänkning per förfrågan åstadkommas genom en kommandoradsalternativ
som:</p>

<p>-cipher <q>ALL:@SECLEVEL=1</q></p>

</p>med den relevanta systemnivåkonfigurationen som hittas i
/etc/ssl/openssl.cnf</p>


<h2>Blandade felrättningar</h2>

<p>Denna uppdatering av den gamla stabila utgåvan lägger till några viktiga felrättningar till följande paket:</p>

<table border=0>
<tr><th>Paket</th>               <th>Orsak</th></tr>
<correction apache-log4j1.2 "Resolve security issues [CVE-2021-4104 CVE-2022-23302 CVE-2022-23305 CVE-2022-23307], by removing support for the JMSSink, JDBCAppender, JMSAppender and Apache Chainsaw modules">
<correction apache-log4j2 "Fix remote code execution issue [CVE-2021-44832]">
<correction atftp "Fix information leak issue [CVE-2021-46671]">
<correction base-files "Update for the 10.12 point release">
<correction beads "Rebuild against updated cimg to fix multiple heap buffer overflows [CVE-2020-25693]">
<correction btrbk "Fix regression in the update for CVE-2021-38173">
<correction cargo-mozilla "New package, backported from Debian 11, to help build new rust versions">
<correction chrony "Allow reading the chronyd configuration file that timemaster(8) generates">
<correction cimg "Fix heap buffer overflow issues [CVE-2020-25693]">
<correction clamav "New upstream stable release; fix denial of service issue [CVE-2022-20698]">
<correction cups "Fix <q>an input validation issue might allow a malicious application to read restricted memory</q> [CVE-2020-10001]">
<correction debian-installer "Rebuild against oldstable-proposed-updates; update kernel ABI to -20">
<correction debian-installer-netboot-images "Rebuild against oldstable-proposed-updates">
<correction detox "Fix processing of large files on ARM architectures">
<correction evolution-data-server "Fix crash on malformed server reponse [CVE-2020-16117]">
<correction flac "Fix out of bounds read issue [CVE-2020-0499]">
<correction gerbv "Fix code execution issue [CVE-2021-40391]">
<correction glibc "Import several fixes from upstream's stable branch; simplify the check for supported kernel versions, as 2.x kernels are no longer supported; support installation on kernels with a release number greater than 255">
<correction gmp "Fix integer and buffer overflow issue [CVE-2021-43618]">
<correction graphicsmagick "Fix buffer overflow issue [CVE-2020-12672]">
<correction htmldoc "Fix out-of-bounds read issue [CVE-2022-0534], buffer overflow issues [CVE-2021-43579 CVE-2021-40985]">
<correction http-parser "Resolve inadvertent ABI break">
<correction icu "Fix <q>pkgdata</q> utility">
<correction intel-microcode "Update included microcode; mitigate some security issues [CVE-2020-8694 CVE-2020-8695 CVE-2021-0127 CVE-2021-0145 CVE-2021-0146 CVE-2021-33120]">
<correction jbig2dec "Fix buffer overflow issue [CVE-2020-12268]">
<correction jtharness "New upstream version to support builds of newer OpenJDK-11 versions">
<correction jtreg "New upstream version to support builds of newer OpenJDK-11 versions">
<correction lemonldap-ng "Fix auth process in password-testing plugins [CVE-2021-20874]; add recommends on gsfonts, fixing captcha">
<correction leptonlib "Fix denial of service issue [CVE-2020-36277], buffer over-read issues [CVE-2020-36278 CVE-2020-36279 CVE-2020-36280 CVE-2020-36281]">
<correction libdatetime-timezone-perl "Update included data">
<correction libencode-perl "Fix a memory leak in Encode.xs">
<correction libetpan "Fix STARTTLS response injection issue [CVE-2020-15953]">
<correction libextractor "Fix invalid read issue [CVE-2019-15531]">
<correction libjackson-json-java "Fix code execution issues [CVE-2017-15095 CVE-2017-7525], XML external entity issues [CVE-2019-10172]">
<correction libmodbus "Fix out of bound read issues [CVE-2019-14462 CVE-2019-14463]">
<correction libpcap "Check PHB header length before using it to allocate memory [CVE-2019-15165]">
<correction libsdl1.2 "Properly handle input focus events; fix buffer overflow issues [CVE-2019-13616 CVE-2019-7637], buffer over-read issues [CVE-2019-7572 CVE-2019-7573 CVE-2019-7574 CVE-2019-7575 CVE-2019-7576 CVE-2019-7577 CVE-2019-7578 CVE-2019-7635 CVE-2019-7636 CVE-2019-7638]">
<correction libxml2 "Fix use-after-free issue [CVE-2022-23308]">
<correction linux "New upstream stable release; [rt] Update to 4.19.233-rt105; increase ABI to 20">
<correction linux-latest "Update to 4.19.0-20 ABI">
<correction linux-signed-amd64 "New upstream stable release; [rt] Update to 4.19.233-rt105; increase ABI to 20">
<correction linux-signed-arm64 "New upstream stable release; [rt] Update to 4.19.233-rt105; increase ABI to 20">
<correction linux-signed-i386 "New upstream stable release; [rt] Update to 4.19.233-rt105; increase ABI to 20">
<correction llvm-toolchain-11 "New package, backported from Debian 11, to help build new rust versions">
<correction lxcfs "Fix misreporting of swap usage">
<correction mailman "Fix cross-site scripting issue [CVE-2021-43331]; fix <q>a list moderator can crack the list admin password encrypted in a CSRF token</q> [CVE-2021-43332]; fix potential CSRF attack against a list admin from a list member or moderator [CVE-2021-44227]; fix regressions in fixes for CVE-2021-42097 and CVE-2021-44227">
<correction mariadb-10.3 "New upstream stable release; security fixes [CVE-2021-35604 CVE-2021-46659 CVE-2021-46661 CVE-2021-46662 CVE-2021-46663 CVE-2021-46664 CVE-2021-46665 CVE-2021-46667 CVE-2021-46668 CVE-2022-24048 CVE-2022-24050 CVE-2022-24051 CVE-2022-24052]">
<correction node-getobject "Fix prototype pollution issue [CVE-2020-28282]">
<correction opensc "Fix out-of-bounds access issues [CVE-2019-15945 CVE-2019-15946], crash due to read of unknown memory [CVE-2019-19479], double free issue [CVE-2019-20792], buffer overflow issues [CVE-2020-26570 CVE-2020-26571 CVE-2020-26572]">
<correction openscad "Fix buffer overflows in STL parser [CVE-2020-28599 CVE-2020-28600]">
<correction openssl "New upstream release">
<correction php-illuminate-database "Fix query binding issue [CVE-2021-21263], SQL injection issue when used with Microsoft SQL Server">
<correction phpliteadmin "Fix cross-site scripting issue [CVE-2021-46709]">
<correction plib "Fix integer overflow issue [CVE-2021-38714]">
<correction privoxy "Fix memory leak [CVE-2021-44540] and cross-site scripting issue [CVE-2021-44543]">
<correction publicsuffix "Update included data">
<correction python-virtualenv "Avoid attempting to install pkg_resources from PyPI">
<correction raptor2 "Fix out of bounds array access issue [CVE-2020-25713]">
<correction ros-ros-comm "Fix denial of service issue [CVE-2021-37146]">
<correction rsyslog "Fix heap overflow issues [CVE-2019-17041 CVE-2019-17042]">
<correction ruby-httpclient "Use system certificate store">
<correction rust-cbindgen "New upstream stable release to support builds of newer firefox-esr and thunderbird versions">
<correction rustc-mozilla "New source package to support building of newer firefox-esr and thunderbird versions">
<correction s390-dasd "Stop passing deprecated -f option to dasdfmt">
<correction spip "Fix cross-site scripting issue">
<correction tzdata "Update data for Fiji and Palestine">
<correction vim "Fix ability to execute code while in restricted mode [CVE-2019-20807], buffer overflow issues [CVE-2021-3770 CVE-2021-3778 CVE-2021-3875], use after free issue [CVE-2021-3796]; remove accidentally included patch">
<correction wavpack "Fix use of uninitialized values [CVE-2019-1010317 CVE-2019-1010319]">
<correction weechat "Fix several denial of service issues [CVE-2020-8955 CVE-2020-9759 CVE-2020-9760 CVE-2021-40516]">
<correction wireshark "Fix several security issues in dissectors [CVE-2021-22207 CVE-2021-22235 CVE-2021-39921 CVE-2021-39922 CVE-2021-39923 CVE-2021-39924 CVE-2021-39928 CVE-2021-39929]">
<correction xterm "Fix buffer overflow issue [CVE-2022-24130]">
<correction zziplib "Fix denial of service issue [CVE-2020-18442]">
</table>


<h2>Säkerhetsuppdateringar</h2>


<p>Denna revision lägger till följande säkerhetsuppdateringar till den gamla stabila utgåvan.
Säkerhetsgruppen har redan släppt bulletiner för alla dessa
uppdateringar:</p>

<table border=0>
<tr><th>Bulletin-ID</th>  <th>Paket</th></tr>
<dsa 2019 4513 samba>
<dsa 2021 4982 apache2>
<dsa 2021 4983 neutron>
<dsa 2021 4985 wordpress>
<dsa 2021 4986 tomcat9>
<dsa 2021 4987 squashfs-tools>
<dsa 2021 4989 strongswan>
<dsa 2021 4990 ffmpeg>
<dsa 2021 4991 mailman>
<dsa 2021 4993 php7.3>
<dsa 2021 4994 bind9>
<dsa 2021 4995 webkit2gtk>
<dsa 2021 4997 tiff>
<dsa 2021 5000 openjdk-11>
<dsa 2021 5001 redis>
<dsa 2021 5004 libxstream-java>
<dsa 2021 5005 ruby-kaminari>
<dsa 2021 5006 postgresql-11>
<dsa 2021 5010 libxml-security-java>
<dsa 2021 5011 salt>
<dsa 2021 5013 roundcube>
<dsa 2021 5014 icu>
<dsa 2021 5015 samba>
<dsa 2021 5016 nss>
<dsa 2021 5018 python-babel>
<dsa 2021 5019 wireshark>
<dsa 2021 5020 apache-log4j2>
<dsa 2021 5021 mediawiki>
<dsa 2021 5022 apache-log4j2>
<dsa 2021 5023 modsecurity-apache>
<dsa 2021 5024 apache-log4j2>
<dsa 2021 5027 xorg-server>
<dsa 2021 5028 spip>
<dsa 2021 5029 sogo>
<dsa 2021 5030 webkit2gtk>
<dsa 2021 5032 djvulibre>
<dsa 2022 5035 apache2>
<dsa 2022 5036 sphinxsearch>
<dsa 2022 5037 roundcube>
<dsa 2022 5038 ghostscript>
<dsa 2022 5039 wordpress>
<dsa 2022 5040 lighttpd>
<dsa 2022 5043 lxml>
<dsa 2022 5047 prosody>
<dsa 2022 5051 aide>
<dsa 2022 5052 usbview>
<dsa 2022 5053 pillow>
<dsa 2022 5056 strongswan>
<dsa 2022 5057 openjdk-11>
<dsa 2022 5059 policykit-1>
<dsa 2022 5060 webkit2gtk>
<dsa 2022 5062 nss>
<dsa 2022 5063 uriparser>
<dsa 2022 5065 ipython>
<dsa 2022 5066 ruby2.5>
<dsa 2022 5071 samba>
<dsa 2022 5072 debian-edu-config>
<dsa 2022 5073 expat>
<dsa 2022 5075 minetest>
<dsa 2022 5076 h2database>
<dsa 2022 5078 zsh>
<dsa 2022 5081 redis>
<dsa 2022 5083 webkit2gtk>
<dsa 2022 5085 expat>
<dsa 2022 5087 cyrus-sasl2>
<dsa 2022 5088 varnish>
<dsa 2022 5093 spip>
<dsa 2022 5096 linux-latest>
<dsa 2022 5096 linux-signed-amd64>
<dsa 2022 5096 linux-signed-arm64>
<dsa 2022 5096 linux-signed-i386>
<dsa 2022 5096 linux>
<dsa 2022 5098 tryton-server>
<dsa 2022 5099 tryton-proteus>
<dsa 2022 5100 nbd>
<dsa 2022 5101 libphp-adodb>
<dsa 2022 5103 openssl>
<dsa 2022 5105 bind9>
</table>


<h2>Borttagna paket</h2>

<p>Följande paket har tagits bort på grund av omständigheter utom vår kontroll:</p>

<table border=0>
<tr><th>Paket</th>               <th>Orsak</th></tr>
<correction angular-maven-plugin "Inte längre användbar">
<correction minify-maven-plugin "Inte längre användbar">

</table>

<h2>Debianinstalleraren</h2>
<p>Installeraren har uppdaterats för att inkludera rättningarna som har inkluderats i den
gamla stabila utgåvan med denna punktutgåva.</p>

<h2>URLer</h2>

<p>Den fullständiga listan på paket som har förändrats i denna revision:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Den aktuella gamla stabila utgåvan:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable/">
</div>

<p>Föreslagna uppdateringar till den gamla stabila utgåvan:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>Information om den gamla stabila utgåvan (versionsfakta, kända problem osv.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>Säkerhetsbulletiner och information:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Om Debian</h2>

<p>Debianprojektet är en grupp utvecklare av Fri mjukvara som
donerar sin tid och kraft för att producera det helt
fria operativsystemet Debian.</p>

<h2>Kontaktinformation</h2>

<p>För ytterligare information, vänligen besök Debians webbplats på
<a href="$(HOME)/">https://www.debian.org/</a>, skicka e-post till
&lt;press@debian.org&gt;, eller kontakta gruppen för stabila utgåvor på
&lt;debian-release@lists.debian.org&gt;.</p>


