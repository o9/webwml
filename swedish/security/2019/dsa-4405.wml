#use wml::debian::translation-check translation="da347ceee9cca800740ef75deed5e600ef8e2b1d" mindelta="1"
<define-tag description>säkerhetsuppdatering</define-tag>
<define-tag moreinfo>
<p>Flera sårbarheter har upptäckts i openjpeg2, öppen-källkodsavkodaren
för JPEG 2000, som kunde utökas till att orsaka en överbelastning
eller möjligen fjärrkodsexekvering.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17480">CVE-2017-17480</a>

	<p>Skrivstackbuffertspill i jp3d och jpwl-kodekarna kan resultera
	i överbelastning eller fjärrkodsexekvering via en skapad jp3d
	eller jpwl-fil.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5785">CVE-2018-5785</a>

	<p>Heltalsspill kan resultera i överbelastning via skapad bmp-fil.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-6616">CVE-2018-6616</a>

	<p>Överdriven iterering kan resultera i en överbelastning via en
	skapad bmp-fil.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14423">CVE-2018-14423</a>

	<p>Division med noll-sårbarheter kan resultera i överbelastning via
	en skapad j2k-fil.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18088">CVE-2018-18088</a>

	<p>Null-pekardereferens kan resultera i överbelastning via en
	skapad bmp-fil.</p></li>

</ul>

<p>För den stabila utgåvan (Stretch) har dessa problem rättats i
version 2.1.2-1.1+deb9u3.</p>

<p>Vi rekommenderar att ni uppgraderar era openjpeg2-paket.</p>

<p>För detaljerad säkerhetsstatus om openjpeg2 vänligen se
dess säkerhetsspårare på
<a href="https://security-tracker.debian.org/tracker/openjpeg2">\
https://security-tracker.debian.org/tracker/openjpeg2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4405.data"
