#use wml::debian::translation-check translation="9264c23e43e374bf590c29166dde81515943b562" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de la mise à jour de Debian 10.12</define-tag>
<define-tag release_date>2022-03-26</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>10</define-tag>
<define-tag codename>Buster</define-tag>
<define-tag revision>10.12</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Le projet Debian a l'honneur d'annoncer la douzième mise à jour de sa
distribution oldstable Debian <release> (nom de code <q><codename></q>).
Tout en réglant quelques problèmes importants, cette mise à jour corrige
principalement des problèmes de sécurité de la version stable. Les annonces de
sécurité ont déjà été publiées séparément et sont simplement référencées dans ce
document.
</p>

<p>
Veuillez noter que cette mise à jour ne constitue pas une nouvelle version de
Debian <release> mais seulement une mise à jour de certains des paquets qu'elle
contient. Il n'est pas nécessaire de jeter les anciens médias de la
version <codename>. Après installation, les paquets peuvent être mis à niveau
vers les versions courantes en utilisant un miroir Debian à jour.
</p>

<p>
Ceux qui installent fréquemment les mises à jour à partir de security.debian.org
n'auront pas beaucoup de paquets à mettre à jour et la plupart des mises à jour
de security.debian.org sont comprises dans cette mise à jour.
</p>

<p>
De nouvelles images d'installation seront prochainement disponibles à leurs
emplacements habituels.
</p>

<p>
Mettre à jour une installation vers cette révision peut se faire en faisant
pointer le système de gestion de paquets sur l'un des nombreux miroirs HTTP de
Debian. Une liste complète des miroirs est disponible à l'adresse :
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Renforcement de la vérification de l'algorithme de signature d'OpenSSL</h2>

<p>La mise à jour d'OpenSSL fournie dans cette version comprend une modification
pour s'assurer que l'algorithme de signature exigé est pris en charge par le
niveau de sécurité actif.</p>

<p>Bien que cela ne devrait pas toucher la plupart des cas d'utilisation, cela
pourrait mener à la génération de messages d'erreur si un algorithme non pris en
charge est exigé – par exemple, l'utilisation de signatures RSA+SHA1 avec le
niveau de sécurité par défaut (niveau 2).</p>

<p>Dans un cas comme celui-là, le niveau de sécurité devra être abaissé explicitement soit pour des requêtes individuelles, soit plus globalement. Cela
peut nécessiter de modifier la configuration des applications. Pour OpenSSL
lui-même, un abaissement à chaque requête peut être obtenu avec une option en
ligne de commande telle que :</p>

<p>-cipher <q>ALL:@SECLEVEL=1</q></p>

<p>avec la configuration adéquate au niveau du système qui se trouve dans
/etc/ssl/openssl.cnf</p>


<h2>Corrections de bogues divers</h2>

<p>
Cette mise à jour de la version oldstable apporte quelques corrections
importantes aux paquets suivants :
</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction apache-log4j1.2 "Problèmes de sécurité [CVE-2021-4104 CVE-2022-23302 CVE-2022-23305 CVE-2022-23307] résolus en supprimant la prise en charge des modules JMSSink, JDBCAppender, JMSAppender et Apache Chainsaw">
<correction apache-log4j2 "Correction d'un problème d'exécution de code à distance [CVE-2021-44832]">
<correction atftp "Correction d'un problème de fuite d'informations [CVE-2021-46671]">
<correction base-files "Mise à jour pour cette version 10.12">
<correction beads "Reconstruction avec cimg mis à jour pour corriger plusieurs dépassements de tampon de tas [CVE-2020-25693]">
<correction btrbk "Correction d'une régression dans la mise à jour pour le CVE-2021-38173">
<correction cargo-mozilla "Nouveau paquet, rétroporté à partir de Debian 11, pour aider la construction avec les nouvelles versions de Rust">
<correction chrony "Lecture permise du fichier de configuration de chronyd que génère timemaster(8)">
<correction cimg "Correction de problèmes de dépassement de tas [CVE-2020-25693]">
<correction clamav "Nouvelle version amont stable ; correction d'un problème de dépassement de tampon [CVE-2022-20698]">
<correction cups "Correction d'un <q>problème de validation d'entrée qui pourrait permettre à une application malveillante de lire de la mémoire sensible</q> [CVE-2020-10001]">
<correction debian-installer "Reconstruction avec oldstable-proposed-updates ; mise à jour de l'ABI du noyau vers la version -20">
<correction debian-installer-netboot-images "Reconstruction avec oldstable-proposed-updates">
<correction detox "Correction du traitement de grands fichiers sur les architectures ARM">
<correction evolution-data-server "Correction de plantage avec une réponse du serveur mal formée [CVE-2020-16117]">
<correction flac "Correction d'un problème de lecture hors limites [CVE-2020-0499]">
<correction gerbv "Correction d'un problème d'exécution de code [CVE-2021-40391]">
<correction glibc "Importation de plusieurs corrections à partir de la branche stable de l'amont ; simplification de la vérification des versions du noyau prises en charge dans la mesure où les noyaux 2.x ne sont plus pris en charge ; prise en charge de l'installation des noyaux avec un numéro supérieur à 255">
<correction gmp "Correction d'un problème de dépassement d'entier et de tampon [CVE-2021-43618]">
<correction graphicsmagick "Correction d'un problème de dépassement de tampon [CVE-2020-12672]">
<correction htmldoc "Correction d'un problème de lecture hors limites [CVE-2022-0534] et de problèmes de dépassement de tampon [CVE-2021-43579 CVE-2021-40985]">
<correction http-parser "Casse involontaire d'ABI résolue">
<correction icu "Correction de l'utilitaire <q>pkgdata</q>">
<correction intel-microcode "Mise en jour du microprogramme inclus ; atténuation de certains problèmes de sécurité [CVE-2020-8694 CVE-2020-8695 CVE-2021-0127 CVE-2021-0145 CVE-2021-0146 CVE-2021-33120]">
<correction jbig2dec "Correction d'un problème de dépassement de tampon [CVE-2020-12268]">
<correction jtharness "Nouvelle version amont pour prendre en charge des constructions avec les nouvelles versions d'OpenJDK-11">
<correction jtreg "Nouvelle version amont pour prendre en charge des constructions avec les nouvelles versions d'OpenJDK-11">
<correction lemonldap-ng "Correction du processus d'authentification dans les greffons de test de mot de passe [CVE-2021-20874] ; ajout d'une recommandation à gsfonts corrigeant captcha">
<correction leptonlib "Correction d'un problème de déni de service [CVE-2020-36277], et de problèmes de lecture excessive de tampon [CVE-2020-36278 CVE-2020-36279 CVE-2020-36280 CVE-2020-36281]">
<correction libdatetime-timezone-perl "Mise à jour des données incluses">
<correction libencode-perl "Correction d'une fuite de mémoire dans Encode.xs">
<correction libetpan "Correction d'un problème d'injection de réponse STARTTLS [CVE-2020-15953]">
<correction libextractor "Correction d'un problème de lecture non valable [CVE-2019-15531]">
<correction libjackson-json-java "Corrections de problèmes d'exécution de code [CVE-2017-15095 CVE-2017-7525] et de problèmes d'entité externe XML [CVE-2019-10172]">
<correction libmodbus "Correction de problèmes de lecture hors limites [CVE-2019-14462 CVE-2019-14463]">
<correction libpcap "Vérification de la longueur de l'entête PHB avant son utilisation pour allouer de la mémoire [CVE-2019-15165]">
<correction libsdl1.2 "Gestion correcte des événements de l'élément actif de saisie ; correction de problèmes de dépassement de tampon [CVE-2019-13616 CVE-2019-7637] et de problèmes de lecture excessive de tampon [CVE-2019-7572 CVE-2019-7573 CVE-2019-7574 CVE-2019-7575 CVE-2019-7576 CVE-2019-7577 CVE-2019-7578 CVE-2019-7635 CVE-2019-7636 CVE-2019-7638]">
<correction libxml2 "Correction d'un problème d'utilisation de mémoire après libération [CVE-2022-23308]">
<correction linux "Nouvelle version amont stable ; [rt] mise à jour vers la version 4.19.233-rt105 ; passage de l'ABI à la version 20">
<correction linux-latest "Mise à jour vers la version 4.19.0-20 de l'ABI">
<correction linux-signed-amd64 "Nouvelle version amont stable ; [rt] mise à jour vers la version 4.19.233-rt105 ; passage de l'ABI à la version 20">
<correction linux-signed-arm64 "Nouvelle version amont stable ; [rt] mise à jour vers la version 4.19.233-rt105 ; passage de l'ABI à la version 20">
<correction linux-signed-i386 "Nouvelle version amont stable ; [rt] mise à jour vers la version 4.19.233-rt105 ; passage de l'ABI à la version 20">
<correction llvm-toolchain-11 "Nouveau paquet, rétroporté à partir de Debian 11, pour aider la construction avec les nouvelles versions de Rust">
<correction lxcfs "Correction d'un mauvais rapport d'utilisation de swap">
<correction mailman "Correction d'un problème de script intersite [CVE-2021-43331] ; correction de <q>la possibilité pour un modérateur de liste de découvrir le mot passe, chiffré dans un jeton CSFR, d'un administrateur de liste</q> [CVE-2021-43332] ; correction d'une possible attaque CSRF à l'encontre d'un administrateur de liste par un membre ou un modérateur de liste [CVE-2021-44227] ; correction de régressions dans les correctifs pour CVE-2021-42097 et  CVE-2021-44227">
<correction mariadb-10.3 "Nouvelle version amont stable ; corrections de sécurité [CVE-2021-35604 CVE-2021-46659 CVE-2021-46661 CVE-2021-46662 CVE-2021-46663 CVE-2021-46664 CVE-2021-46665 CVE-2021-46667 CVE-2021-46668 CVE-2022-24048 CVE-2022-24050 CVE-2022-24051 CVE-2022-24052]">
<correction node-getobject "Correction d'un problème de pollution de prototype [CVE-2020-28282]">
<correction opensc "Correction de problèmes d'accès hors limites [CVE-2019-15945 CVE-2019-15946], d'un plantage dû à la lecture de mémoire inconnue [CVE-2019-19479], d'un problème de double libération de mémoire [CVE-2019-20792] et de problèmes de dépassement de tampon [CVE-2020-26570 CVE-2020-26571 CVE-2020-26572]">
<correction openscad "Correction de dépassements de tampon dans l'analyseur STL [CVE-2020-28599 CVE-2020-28600]">
<correction openssl "Nouvelle version amont">
<correction php-illuminate-database "Correction d'un problème de liaison de requête [CVE-2021-21263] et d'un problème d'injection SQL lors d'utilisation avec Microsoft SQL Server">
<correction phpliteadmin "Correction d'un problème de script intersite [CVE-2021-46709]">
<correction plib "correction d'un problème de débordement d'entier [CVE-2021-38714]">
<correction privoxy "Correction d'une fuite de mémoire [CVE-2021-44540] et d'un problème de script intersite [CVE-2021-44543]">
<correction publicsuffix "Mise à jour des données incluses">
<correction python-virtualenv "Tentative évitée d'installation de pkg_resources à partir de PyPI">
<correction raptor2 "Correction d'un problème d'accès à un tableau hors limites [CVE-2020-25713]">
<correction ros-ros-comm "Correction d'un problème de déni de service [CVE-2021-37146]">
<correction rsyslog "Correction de problèmes de dépassement de tas [CVE-2019-17041 CVE-2019-17042]">
<correction ruby-httpclient "Utilisation du magasin de certifications du système">
<correction rust-cbindgen "Nouveau paquet source pour prendre en charge la construction des dernières versions de firefox-esr et de thunderbird">
<correction rustc-mozilla "Nouveau paquet source pour prendre en charge la construction des dernières versions de firefox-esr et de thunderbird">
<correction s390-dasd "Plus de transmission de l'option obsolète -f à dasdfmt">
<correction spip "Correction d'un problème de script intersite">
<correction tzdata "Mise à jour des données pour les Fidji et la Palestine">
<correction vim "Correction de la possibilité d'exécuter du code dans le mode restreint [CVE-2019-20807], de problèmes de dépassement de tampon [CVE-2021-3770 CVE-2021-3778 CVE-2021-3875] et d'un problème d'utilisation de mémoire après libération [CVE-2021-3796] ; suppression d'un correctif inclus accidentellement">
<correction wavpack "Correction d'une utilisation de valeurs non initialisées [CVE-2019-1010317 CVE-2019-1010319]">
<correction weechat "Correction de plusieurs problèmes de déni de service [CVE-2020-8955 CVE-2020-9759 CVE-2020-9760 CVE-2021-40516]">
<correction wireshark "Correction de plusieurs problèmes de sécurité dans les dissecteurs [CVE-2021-22207 CVE-2021-22235 CVE-2021-39921 CVE-2021-39922 CVE-2021-39923 CVE-2021-39924 CVE-2021-39928 CVE-2021-39929]">
<correction xterm "Correction d'un problème de dépassement de tampon [CVE-2022-24130]">
<correction zziplib "Correction d'un problème de déni de service [CVE-2020-18442]">
</table>


<h2>Mises à jour de sécurité</h2>


<p>
Cette révision ajoute les mises à jour de sécurité suivantes à la version
oldstable. L'équipe de sécurité a déjà publié une annonce pour chacune de ces
mises à jour :
</p>

<table border=0>
<tr><th>Identifiant</th>  <th>Paquet</th></tr>
<dsa 2019 4513 samba>
<dsa 2021 4982 apache2>
<dsa 2021 4983 neutron>
<dsa 2021 4985 wordpress>
<dsa 2021 4986 tomcat9>
<dsa 2021 4987 squashfs-tools>
<dsa 2021 4989 strongswan>
<dsa 2021 4990 ffmpeg>
<dsa 2021 4991 mailman>
<dsa 2021 4993 php7.3>
<dsa 2021 4994 bind9>
<dsa 2021 4995 webkit2gtk>
<dsa 2021 4997 tiff>
<dsa 2021 5000 openjdk-11>
<dsa 2021 5001 redis>
<dsa 2021 5004 libxstream-java>
<dsa 2021 5005 ruby-kaminari>
<dsa 2021 5006 postgresql-11>
<dsa 2021 5010 libxml-security-java>
<dsa 2021 5011 salt>
<dsa 2021 5013 roundcube>
<dsa 2021 5014 icu>
<dsa 2021 5015 samba>
<dsa 2021 5016 nss>
<dsa 2021 5018 python-babel>
<dsa 2021 5019 wireshark>
<dsa 2021 5020 apache-log4j2>
<dsa 2021 5021 mediawiki>
<dsa 2021 5022 apache-log4j2>
<dsa 2021 5023 modsecurity-apache>
<dsa 2021 5024 apache-log4j2>
<dsa 2021 5027 xorg-server>
<dsa 2021 5028 spip>
<dsa 2021 5029 sogo>
<dsa 2021 5030 webkit2gtk>
<dsa 2021 5032 djvulibre>
<dsa 2022 5035 apache2>
<dsa 2022 5036 sphinxsearch>
<dsa 2022 5037 roundcube>
<dsa 2022 5038 ghostscript>
<dsa 2022 5039 wordpress>
<dsa 2022 5040 lighttpd>
<dsa 2022 5043 lxml>
<dsa 2022 5047 prosody>
<dsa 2022 5051 aide>
<dsa 2022 5052 usbview>
<dsa 2022 5053 pillow>
<dsa 2022 5056 strongswan>
<dsa 2022 5057 openjdk-11>
<dsa 2022 5059 policykit-1>
<dsa 2022 5060 webkit2gtk>
<dsa 2022 5062 nss>
<dsa 2022 5063 uriparser>
<dsa 2022 5065 ipython>
<dsa 2022 5066 ruby2.5>
<dsa 2022 5071 samba>
<dsa 2022 5072 debian-edu-config>
<dsa 2022 5073 expat>
<dsa 2022 5075 minetest>
<dsa 2022 5076 h2database>
<dsa 2022 5078 zsh>
<dsa 2022 5081 redis>
<dsa 2022 5083 webkit2gtk>
<dsa 2022 5085 expat>
<dsa 2022 5087 cyrus-sasl2>
<dsa 2022 5088 varnish>
<dsa 2022 5093 spip>
<dsa 2022 5096 linux-latest>
<dsa 2022 5096 linux-signed-amd64>
<dsa 2022 5096 linux-signed-arm64>
<dsa 2022 5096 linux-signed-i386>
<dsa 2022 5096 linux>
<dsa 2022 5098 tryton-server>
<dsa 2022 5099 tryton-proteus>
<dsa 2022 5100 nbd>
<dsa 2022 5101 libphp-adodb>
<dsa 2022 5103 openssl>
<dsa 2022 5105 bind9>
</table>


<h2>Paquets supprimés</h2>

<p>Les paquets suivants ont été supprimés à cause de circonstances hors de
notre contrôle :</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction angular-maven-plugin "Plus nécessaire">
<correction minify-maven-plugin "Plus nécessaire">

</table>

<h2>Installateur Debian</h2>
<p>
L'installateur a été mis à jour pour inclure les correctifs incorporés dans
cette version de oldstable.
</p>

<h2>URL</h2>

<p>
Liste complète des paquets qui ont été modifiés dans cette version :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>
Adresse de l'actuelle distribution oldstable :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable/">
</div>

<p>
Mises à jour proposées à la distribution oldstable :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>
Informations sur la distribution oldstable (notes de publication, <i>errata</i>, etc.) :
</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>
Annonces et informations de sécurité :
</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>À propos de Debian</h2>
<p>
Le projet Debian est une association de développeurs de logiciels libres qui offrent volontairement leur temps et leurs efforts pour produire le système d'exploitation complètement libre Debian.
</p>

<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un courrier électronique à &lt;press@debian.org&gt; ou contactez l'équipe de publication de la version stable à &lt;debian-release@lists.debian.org&gt;.
</p>
