#use wml::debian::translation-check translation="1d1f8d159bd57a26b5a8603a6dfc4a1937981b1c" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Samba, un serveur de
fichier SMB/CIFS, d'impression et de connexion pour Unix. Le projet
« Common Vulnerabilities and Exposures » (CVE) identifie les problèmes
suivants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14629">CVE-2018-14629</a>

<p>Florian Stuelpner a découvert que Samba est vulnérable à une récursion
infinie de requêtes provoquée par des boucles de CNAME, avec pour
conséquence un déni de service.</p>

<p><a href="https://www.samba.org/samba/security/CVE-2018-14629.html">\
https://www.samba.org/samba/security/CVE-2018-14629.html</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16841">CVE-2018-16841</a>

<p>Alex MacCuish a découvert qu'un utilisateur avec un certificat valable
ou une carte à puce peut planter le KDC de Samba AD DC s'il est configuré
pour accepter l'authentification par carte à puce.</p>

<p><a href="https://www.samba.org/samba/security/CVE-2018-16841.html">\
https://www.samba.org/samba/security/CVE-2018-16841.html</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16851">CVE-2018-16851</a>

<p>Garming Sam de l'équipe Samba et Catalyst ont découvert une
vulnérabilité de déréférencement de pointeur NULL dans le serveur LDAP de
Samba AD DC permettant à un utilisateur capable de lire plus de 256 Mo
d'entrées LDAP de planter le serveur LDAP de Samba AD DC.</p>

<p><a href="https://www.samba.org/samba/security/CVE-2018-16851.html">\
https://www.samba.org/samba/security/CVE-2018-16851.html</a></p></li>

</ul>

<p>Pour la distribution stable (Stretch), ces problèmes ont été corrigés
dans la version 2:4.5.12+dfsg-2+deb9u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets samba.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de samba, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/samba">\
https://security-tracker.debian.org/tracker/samba</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2018/dsa-4345.data"
# $Id: $
