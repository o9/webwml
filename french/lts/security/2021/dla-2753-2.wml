#use wml::debian::translation-check translation="400b6f1948a3d2b7dd50665c79ac919a5ac9cd6b" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il a été découvert que le correctif pour le
<a href="https://security-tracker.debian.org/tracker/CVE-2021-3592">CVE-2021-3592</a>
introduisait une régression qui empêchait les connexions sur le système hôte.
Puisqu’il n’existe pas de solution imminente au problème, le correctif pour le
<a href="https://security-tracker.debian.org/tracker/CVE-2021-3592">CVE-2021-3592</a>
a été abandonné. Des paquets mis à jour sont maintenant disponibles pour
corriger ce problème.</p>

<p>Pour Debian 9 « Stretch », ce problème a été corrigé dans
la version 1:2.8+dfsg-6+deb9u16.</p>

<p>Nous vous recommandons de mettre à jour vos paquets qemu.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de qemu,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/qemu">\
https://security-tracker.debian.org/tracker/qemu</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2753-2.data"
# $Id: $
