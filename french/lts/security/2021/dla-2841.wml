#use wml::debian::translation-check translation="5bf5786a60b59fb5ccc9c616f07cdd64f6d30733" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il y avait un problème de dépassement d'entier dans runc, l'exécutable
du projet Open Container, souvent utilisé avec Docker.</p>

<p>Le champ de longueur « bytemsg » de Netlink pouvait permettre à un
attaquant d'écraser les configurations de conteneur basées sur Netlink.
Cette vulnérabilité nécessitait que l'attaquant ait un certain contrôle sur
la configuration du conteneur, mais aurait pu permettre à l'attaquant de
contourner les restrictions de l'espace de noms du conteneur en ajoutant
simplement leur propre charge utile de Netlink qui désactive tous les
espaces de noms.</p>

<ul>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43784">CVE-2021-43784</a></li>
</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 0.1.1+dfsg1-2+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets runc.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2841.data"
# $Id: $
