#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilité de sécurité ont été trouvées dans Bouncy
Castle, une implémentation en Java d’algorithmes de chiffrement.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1000338">CVE-2016-1000338</a>

<p>DSA ne valide pas complètement les signatures d’encodage ASN.1 lors de
vérification. Il est possible d’injecter des éléments supplémentaires dans la
séquence réalisant la signature et la conserver valable, ce qui peut dans
certains cas permettre l’introduction de données <q>invisibles</q> dans la
structure signée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1000339">CVE-2016-1000339</a>

<p>Précédemment, la classe de moteur primaire utilisée pour AES était
AESFastEngine. Dû à l’approche hautement guidée par des tables utilisée dans
l’algorithme, il advient que si le canal des données sur le CPU peut être
contrôlé, les accès à la table de recherche sont suffisants pour divulguer des
informations sur la clef utilisée. Il existait aussi une brèche dans AESEngine
quoiqu’elle soit considérablement moindre. AESEngine a été modifié pour
supprimer tout signe de brèche et est désormais la classe AES primaire pour le
fournisseur de JCE de Bouncy Castle. L’utilisation d’AESFastEngine est maintenant
seulement recommandée où cela semble plus approprié.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1000341">CVE-2016-1000341</a>

<p>La génération de signatures DSA est vulnérable à des attaques temporelles.
Lorsque les horodatages peuvent être observés de près lors de la génération de
signatures, le manque d’offuscation peut permettre à un attaquant d’obtenir des
informations sur la valeur k de la signature et en définitive la valeur privée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1000342">CVE-2016-1000342</a>

<p>ECDSA ne valide pas complètement les signatures d’encodage ASN.1 lors de
vérification. Il est possible d’injecter des éléments supplémentaires dans la
séquence réalisant la signature et et la conserver valable, ce qui peut dans
certains cas permettre l’introduction de données <q>invisibles</q> dans la
structure signée.</p></li>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1000343">CVE-2016-1000343</a>

<p>Le générateur de paires de clefs DSA crée une clef privée faible s’il est
utilisé avec les valeurs par défaut. Si le générateur de paires de clefs JCA
n’est pas initialisé explicitement avec des paramètres DSA, les versions 1.55 et précédentes
génèrent une valeur privée en supposant une taille de clef de 1024 bits. Dans les
publications précédentes, cela peut être réglé en passant explicitement des
paramètres au générateur de paires de clefs.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1000345">CVE-2016-1000345</a>

<p>Le mode CBC DHIES/ECIES est vulnérable à une attaque d’oracle par
remplissage. Dans un environnement où les horodatages peuvent être facilement
observés, il est possible avec suffisamment d’observations d’identifier où le
déchiffrement échoue à cause du remplissage.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1000346">CVE-2016-1000346</a>

<p>Dans le fournisseur de JCE de Bouncy Castle, la clef publique DH de l’autre
partie n’est pas totalement validée. Cela peut causer des problèmes car des
clefs non valables peuvent être utilisées pour révéler des détails sur la clef
privée de l’autre partie où Diffie-Hellman statique est utilisé. Dans
cette publication, les paramètres de clef sont vérifiés par calcul d’agrément.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.49+dfsg-3+deb8u3.</p>
<p>Nous vous recommandons de mettre à jour vos paquets bouncycastle.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1418.data"
# $Id: $
