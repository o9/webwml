#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Ming :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11732">CVE-2017-11732</a>

<p>une vulnérabilité de dépassement de tas dans la fonction dcputs
(util/decompile.c) dans Ming <= 0.4.8. Cela permet à des attaquants de
provoquer un déni de service à l'aide d'un fichier SWF contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16883">CVE-2017-16883</a>

<p>une vulnérabilité de déréférencement de pointeur NULL dans la fonction
outputSWF_TEXT_RECORD (util/outputscript.c) dans Ming <= 0.4.8. Cela permet
à des attaquants de provoquer un déni de service à l'aide d'un fichier SWF
contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16898">CVE-2017-16898</a>

<p>une vulnérabilité de dépassement de tampon global dans la fonction
printMP3Headers (util/listmp3.c) dans Ming <= 0.4.8. Cela permet à des
attaquants de provoquer un déni de service à l'aide d'un fichier SWF
contrefait.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 1:0.4.4-1.1+deb7u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ming.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1240.data"
# $Id: $
