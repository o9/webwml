#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Sze Yiu Chau et son équipe de l’université de Purdue et l’université d’Iowa
 ont trouvé plusieurs problèmes de sécurité dans le greffon gmp pour strongSwan,
une suite IKE/IPsec.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16151">CVE-2018-16151</a>

<p>L’analyseur d’OID dans le code ASN.1 dans gmp permet n’importe quel nombre
d’octets aléatoires après un OID valable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16152">CVE-2018-16152</a>

<p>L’analyseur d’algorithmIdentifier dans le code ASN.1 dans gmp n’impose pas une
valeur NULL pour le paramètre facultatif qui n’est pas utilisé avec certains
algorithmes PKCS#1.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 5.2.1-6+deb8u7.</p>
<p>Nous vous recommandons de mettre à jour vos paquets strongswan.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1522.data"
# $Id: $
