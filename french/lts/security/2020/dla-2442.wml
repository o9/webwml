#use wml::debian::translation-check translation="9d257d8ffbbad3fe2a83b371853e157fc6c980b7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>golang-go.crypto a été récemment mis à jour pour le
<a href="https://security-tracker.debian.org/tracker/CVE-2019-11840">CVE-2019-11840</a>.
Cela conduit à ce que tous les paquets utilisant le code affecté soient
recompilés pour tenir compte du correctif de sécurité.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11840">CVE-2019-11840</a>

<p>Un problème a été découvert dans la bibliothèque complémentaire de
chiffrement de Go, c'est-à-dire, golang-googlecode-go-crypto. Si plus de 256 GiB
de séquences de clés sont générés ou si le compteur dépasse 32 bits,
l’implémentation d’amd64 générera d’abord une sortie incorrecte, puis reviendra
à la séquence précédente. Des répétitions d’octets de séquence peuvent conduire
à une perte de confidentialité dans les applications de chiffrement ou à une
prédictibilité dans les applications CSPRNG.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la
version 3 0.0.7-1+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets obfs4proxy.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de obfs4proxy, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/obfs4proxy">https://security-tracker.debian.org/tracker/obfs4proxy</a></p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2442.data"
# $Id: $
