#use wml::debian::translation-check translation="99aaabe6a2f6e9b687d7c080767680f2c7b772b1" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Tim Starling a découvert deux vulnérabilités dans firejail, un programme de
bac à sable pour restreindre l’environnement d’applications non fiables.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-17367">CVE-2020-17367</a>

<p>Il a été signalé que firejail ne respecte pas le séparateur de fin d’option
(« -- »), permettant à un attaquant contrôlant les options de ligne de commande
de l’application dans le bac à sable d’écrire des données dans un fichier
spécifié.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-17368">CVE-2020-17368</a>

<p>Il a été signalé que firejail lors de la redirection de la sortie à l’aide de
--output ou --output-stderr, concatène tous les arguments de ligne de commande
en une seule chaîne passée à un interpréteur. Un attaquant contrôlant les
options de ligne de commande de l’application dans le bac à sable pourrait
exploiter ce défaut pour exécuter d’autres commandes arbitraires.</p>


<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 0.9.44.8-2+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets firejail.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de firejail, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/firejail">https://security-tracker.debian.org/tracker/firejail</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</ul>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2336.data"
# $Id: $
