#use wml::debian::translation-check translation="55d1ac616e9ec6fe42ad1680e45c2ce133b85547" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Le code ne vérifiait pas qu'il n'y avait pas de dépassement d'entier
avant d'essayer de redimensionner un tampon. Un fichier contrefait pour
l'occasion pourrait avoir pour conséquence l'utilisation de mémoire
au-delà de la fin du tampon alloué.</p>

<p>Le CVE de sécurité pour ce problème (<a href="https://security-tracker.debian.org/tracker/CVE-2016-4563">CVE-2016-4563</a>) de même que les
<a href="https://security-tracker.debian.org/tracker/CVE-2016-4562">CVE-2016-4562</a> et
<a href="https://security-tracker.debian.org/tracker/CVE-2016-4564">CVE-2016-4564</a>
étaient basés sur une annonce de sécurité qui ne sera pas rendue publique.
Ce correctif de sécurité a été généré en se basant uniquement sur les
informations superficielles de modification de code qui sont publiques dans
GitHub. Pour davantage d'information sur cela, consultez :
<a href="http://seclists.org/oss-sec/2016/q2/476">http://seclists.org/oss-sec/2016/q2/476</a></p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 8:6.7.7.10-5+deb7u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets imagemagick.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-517.data"
# $Id: $
