# l10n webwml Catalan template.
# Copyright (C) 2002, 2004 Free Software Foundation, Inc.
# Jordi Mallach <jordi@debian.org>, 2002.
# Guillem Jover <guillem@debian.org>, 2004, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: webwml\n"
"PO-Revision-Date: 2009-02-14 16:09+0200\n"
"Last-Translator: Guillem Jover <guillem@debian.org>\n"
"Language-Team: Catalan <debian-l10n-catalan@lists.debian.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/international/l10n/dtc.def:10
msgid "File"
msgstr "Fitxer"

#: ../../english/international/l10n/dtc.def:14
msgid "Package"
msgstr "Paquet"

#: ../../english/international/l10n/dtc.def:18
msgid "Score"
msgstr "Puntuació"

#: ../../english/international/l10n/dtc.def:22
msgid "Translator"
msgstr "Traductor"

#: ../../english/international/l10n/dtc.def:26
msgid "Team"
msgstr "Equip"

#: ../../english/international/l10n/dtc.def:30
msgid "Date"
msgstr "Data"

#: ../../english/international/l10n/dtc.def:34
msgid "Status"
msgstr "Estat"

#: ../../english/international/l10n/dtc.def:38
msgid "Strings"
msgstr "Cadenes"

#: ../../english/international/l10n/dtc.def:42
msgid "Bug"
msgstr "Informe d'error"

#: ../../english/international/l10n/dtc.def:49
msgid "<get-var lang />, as spoken in <get-var country />"
msgstr "<get-var lang />, com es parla en <get-var country />"

#: ../../english/international/l10n/dtc.def:54
msgid "Unknown language"
msgstr "Llengua desconeguda"

#: ../../english/international/l10n/dtc.def:64
msgid "This page was generated with data collected on: <get-var date />."
msgstr ""
"Aquesta pàgina ha estat generada amb les dades recollides el <get-var date /"
">."

#: ../../english/international/l10n/dtc.def:69
msgid "Before working on these files, make sure they are up to date!"
msgstr ""
"Abans de treballar amb aquests fitxers assegureu-vos de que estan al dia!"

#: ../../english/international/l10n/dtc.def:79
msgid "Section: <get-var name />"
msgstr "Secció: <get-var name />"

#: ../../english/international/l10n/menu.def:10
msgid "L10n"
msgstr "Localització"

#: ../../english/international/l10n/menu.def:14
msgid "Language list"
msgstr "Llista de les llengües"

#: ../../english/international/l10n/menu.def:18
msgid "Ranking"
msgstr "Classificació"

#: ../../english/international/l10n/menu.def:22
msgid "Hints"
msgstr "Consells"

#: ../../english/international/l10n/menu.def:26
msgid "Errors"
msgstr "Errors"

#: ../../english/international/l10n/menu.def:30
msgid "POT files"
msgstr "fitxers POT"

#: ../../english/international/l10n/menu.def:34
msgid "Hints for translators"
msgstr "Consells per als traductors"

#~ msgid "Original templates"
#~ msgstr "Plantilla original"

#~ msgid "Translated templates"
#~ msgstr "Plantilla traduïda"
