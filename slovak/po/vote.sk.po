# Translation of vote.sk.po
# Copyright (C)
# This file is distributed under the same license as the webwml package.
# Ivan Masár <helix84@centrum.sk>, 2009, 2017.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"Report-Msgid-Bugs-To: debian-www@lists.debian.org\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2017-07-12 16:39+0200\n"
"Last-Translator: Ivan Masár <helix84@centrum.sk>\n"
"Language-Team: x\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Virtaal 0.7.1\n"

#: ../../english/template/debian/votebar.wml:13
msgid "Date"
msgstr "Dátum"

#: ../../english/template/debian/votebar.wml:16
msgid "Time Line"
msgstr "Časový rozvrh"

#: ../../english/template/debian/votebar.wml:19
msgid "Summary"
msgstr "Zhrnutie"

#: ../../english/template/debian/votebar.wml:22
msgid "Nominations"
msgstr "Nominácie"

#: ../../english/template/debian/votebar.wml:25
#, fuzzy
msgid "Withdrawals"
msgstr "Stiahnuté"

#: ../../english/template/debian/votebar.wml:28
msgid "Debate"
msgstr "Debata"

#: ../../english/template/debian/votebar.wml:31
msgid "Platforms"
msgstr "Platformy"

#: ../../english/template/debian/votebar.wml:34
msgid "Proposer"
msgstr "Navrhovateľ"

#: ../../english/template/debian/votebar.wml:37
msgid "Proposal A Proposer"
msgstr "Návrh A navrhuje"

#: ../../english/template/debian/votebar.wml:40
msgid "Proposal B Proposer"
msgstr "Návrh B navrhuje"

#: ../../english/template/debian/votebar.wml:43
msgid "Proposal C Proposer"
msgstr "Návrh C navrhuje"

#: ../../english/template/debian/votebar.wml:46
msgid "Proposal D Proposer"
msgstr "Návrh D navrhuje"

#: ../../english/template/debian/votebar.wml:49
msgid "Proposal E Proposer"
msgstr "Návrh E navrhuje"

#: ../../english/template/debian/votebar.wml:52
msgid "Proposal F Proposer"
msgstr "Návrh F navrhuje"

#: ../../english/template/debian/votebar.wml:55
#, fuzzy
msgid "Proposal G Proposer"
msgstr "Návrh A navrhuje"

#: ../../english/template/debian/votebar.wml:58
#, fuzzy
msgid "Proposal H Proposer"
msgstr "Návrh A navrhuje"

#: ../../english/template/debian/votebar.wml:61
msgid "Seconds"
msgstr "Sekunduje"

#: ../../english/template/debian/votebar.wml:64
msgid "Proposal A Seconds"
msgstr "Návrh A sekunduje"

#: ../../english/template/debian/votebar.wml:67
msgid "Proposal B Seconds"
msgstr "Návrh B sekunduje"

#: ../../english/template/debian/votebar.wml:70
msgid "Proposal C Seconds"
msgstr "Návrh C sekunduje"

#: ../../english/template/debian/votebar.wml:73
msgid "Proposal D Seconds"
msgstr "Návrh D sekunduje"

#: ../../english/template/debian/votebar.wml:76
msgid "Proposal E Seconds"
msgstr "Návrh E sekunduje"

#: ../../english/template/debian/votebar.wml:79
msgid "Proposal F Seconds"
msgstr "Návrh F sekunduje"

#: ../../english/template/debian/votebar.wml:82
#, fuzzy
msgid "Proposal G Seconds"
msgstr "Návrh A sekunduje"

#: ../../english/template/debian/votebar.wml:85
#, fuzzy
msgid "Proposal H Seconds"
msgstr "Návrh A sekunduje"

#: ../../english/template/debian/votebar.wml:88
msgid "Opposition"
msgstr "Opozícia"

#: ../../english/template/debian/votebar.wml:91
msgid "Text"
msgstr "Text"

#: ../../english/template/debian/votebar.wml:94
msgid "Proposal A"
msgstr "Návrh A"

#: ../../english/template/debian/votebar.wml:97
msgid "Proposal B"
msgstr "Návrh B"

#: ../../english/template/debian/votebar.wml:100
msgid "Proposal C"
msgstr "Návrh C"

#: ../../english/template/debian/votebar.wml:103
msgid "Proposal D"
msgstr "Návrh D"

#: ../../english/template/debian/votebar.wml:106
msgid "Proposal E"
msgstr "Návrh E"

#: ../../english/template/debian/votebar.wml:109
msgid "Proposal F"
msgstr "Návrh F"

#: ../../english/template/debian/votebar.wml:112
#, fuzzy
msgid "Proposal G"
msgstr "Návrh A"

#: ../../english/template/debian/votebar.wml:115
#, fuzzy
msgid "Proposal H"
msgstr "Návrh A"

#: ../../english/template/debian/votebar.wml:118
msgid "Choices"
msgstr "Možnosti"

#: ../../english/template/debian/votebar.wml:121
msgid "Amendment Proposer"
msgstr "Dodatok navrhuje"

#: ../../english/template/debian/votebar.wml:124
msgid "Amendment Seconds"
msgstr "Dodatok sekunduje"

#: ../../english/template/debian/votebar.wml:127
msgid "Amendment Text"
msgstr "Text dodatku"

#: ../../english/template/debian/votebar.wml:130
msgid "Amendment Proposer A"
msgstr "Dodatok navrhuje A"

#: ../../english/template/debian/votebar.wml:133
msgid "Amendment Seconds A"
msgstr "Dodatok sekunduje A"

#: ../../english/template/debian/votebar.wml:136
msgid "Amendment Text A"
msgstr "Text dodatku A"

#: ../../english/template/debian/votebar.wml:139
msgid "Amendment Proposer B"
msgstr "Dodatok navrhuje B"

#: ../../english/template/debian/votebar.wml:142
msgid "Amendment Seconds B"
msgstr "Dodatok sekunduje B"

#: ../../english/template/debian/votebar.wml:145
msgid "Amendment Text B"
msgstr "Text dodatku B"

#: ../../english/template/debian/votebar.wml:148
msgid "Amendment Proposer C"
msgstr "Dodatok navrhuje C"

#: ../../english/template/debian/votebar.wml:151
msgid "Amendment Seconds C"
msgstr "Dodatok sekunduje C"

#: ../../english/template/debian/votebar.wml:154
msgid "Amendment Text C"
msgstr "Text dodatku C"

#: ../../english/template/debian/votebar.wml:157
msgid "Amendments"
msgstr "Dodatky"

#: ../../english/template/debian/votebar.wml:160
msgid "Proceedings"
msgstr "Jednanie"

#: ../../english/template/debian/votebar.wml:163
msgid "Majority Requirement"
msgstr "Požiadavka väčšiny"

#: ../../english/template/debian/votebar.wml:166
msgid "Data and Statistics"
msgstr "Údaje a štatistika"

#: ../../english/template/debian/votebar.wml:169
msgid "Quorum"
msgstr "Kvórum"

#: ../../english/template/debian/votebar.wml:172
msgid "Minimum Discussion"
msgstr "Minimálna diskusia"

#: ../../english/template/debian/votebar.wml:175
msgid "Ballot"
msgstr "Počet hlasov"

#: ../../english/template/debian/votebar.wml:178
msgid "Forum"
msgstr "Fórum"

#: ../../english/template/debian/votebar.wml:181
msgid "Outcome"
msgstr "Výsledok"

#: ../../english/template/debian/votebar.wml:185
msgid "Waiting&nbsp;for&nbsp;Sponsors"
msgstr "Čaká&nbsp;sa&nbsp;na&nbsp;sponzorov"

#: ../../english/template/debian/votebar.wml:188
msgid "In&nbsp;Discussion"
msgstr "Diskutuje&nbsp;sa"

#: ../../english/template/debian/votebar.wml:191
msgid "Voting&nbsp;Open"
msgstr "Hlasovanie&nbsp;otvorené"

#: ../../english/template/debian/votebar.wml:194
msgid "Decided"
msgstr "Rozhodnuté"

#: ../../english/template/debian/votebar.wml:197
msgid "Withdrawn"
msgstr "Stiahnuté"

#: ../../english/template/debian/votebar.wml:200
msgid "Other"
msgstr "Iné"

#: ../../english/template/debian/votebar.wml:204
msgid "Home&nbsp;Vote&nbsp;Page"
msgstr "Domov&nbsp;Stránka&nbsp;Hlasovania"

#: ../../english/template/debian/votebar.wml:207
msgid "How&nbsp;To"
msgstr "Ako&nbsp;na&nbsp;to"

#: ../../english/template/debian/votebar.wml:210
msgid "Submit&nbsp;a&nbsp;Proposal"
msgstr "Poslať&nbsp;návrh"

#: ../../english/template/debian/votebar.wml:213
msgid "Amend&nbsp;a&nbsp;Proposal"
msgstr "Doplniť&nbsp;návrh"

#: ../../english/template/debian/votebar.wml:216
msgid "Follow&nbsp;a&nbsp;Proposal"
msgstr "Sledovať&nbsp;návrh"

#: ../../english/template/debian/votebar.wml:219
msgid "Read&nbsp;a&nbsp;Result"
msgstr "Prečítať&nbsp;výsledok"

#: ../../english/template/debian/votebar.wml:222
msgid "Vote"
msgstr "Hlasovať"
