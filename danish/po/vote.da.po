msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2006-08-12 17:53+0200\n"
"Last-Translator: unknown\n"
"Language-Team: unknown\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/template/debian/votebar.wml:13
msgid "Date"
msgstr "Dato"

#: ../../english/template/debian/votebar.wml:16
msgid "Time Line"
msgstr "Tidslinje"

#: ../../english/template/debian/votebar.wml:19
msgid "Summary"
msgstr "Resume"

#: ../../english/template/debian/votebar.wml:22
msgid "Nominations"
msgstr "Nomineringer"

#: ../../english/template/debian/votebar.wml:25
msgid "Withdrawals"
msgstr "Tilbagetrukkede"

#: ../../english/template/debian/votebar.wml:28
msgid "Debate"
msgstr "Debat"

#: ../../english/template/debian/votebar.wml:31
msgid "Platforms"
msgstr "Platforme"

#: ../../english/template/debian/votebar.wml:34
msgid "Proposer"
msgstr "Forslagsstiller"

#: ../../english/template/debian/votebar.wml:37
msgid "Proposal A Proposer"
msgstr "Forslags A's forslagsstiller"

#: ../../english/template/debian/votebar.wml:40
msgid "Proposal B Proposer"
msgstr "Forslags B's forslagsstiller"

#: ../../english/template/debian/votebar.wml:43
msgid "Proposal C Proposer"
msgstr "Forslags C's forslagsstiller"

#: ../../english/template/debian/votebar.wml:46
msgid "Proposal D Proposer"
msgstr "Forslags D's forslagsstiller"

#: ../../english/template/debian/votebar.wml:49
msgid "Proposal E Proposer"
msgstr "Forslags E's forslagsstiller"

#: ../../english/template/debian/votebar.wml:52
msgid "Proposal F Proposer"
msgstr "Forslags F's forslagsstiller"

#: ../../english/template/debian/votebar.wml:55
msgid "Proposal G Proposer"
msgstr "Forslags G's forslagsstiller"

#: ../../english/template/debian/votebar.wml:58
msgid "Proposal H Proposer"
msgstr "Forslags H's forslagsstiller"

#: ../../english/template/debian/votebar.wml:61
msgid "Seconds"
msgstr "Støtter"

#: ../../english/template/debian/votebar.wml:64
msgid "Proposal A Seconds"
msgstr "Forslags A's støtter"

#: ../../english/template/debian/votebar.wml:67
msgid "Proposal B Seconds"
msgstr "Forslags B's støtter"

#: ../../english/template/debian/votebar.wml:70
msgid "Proposal C Seconds"
msgstr "Forslags C's støtter"

#: ../../english/template/debian/votebar.wml:73
msgid "Proposal D Seconds"
msgstr "Forslags D's støtter"

#: ../../english/template/debian/votebar.wml:76
msgid "Proposal E Seconds"
msgstr "Forslags E's støtter"

#: ../../english/template/debian/votebar.wml:79
msgid "Proposal F Seconds"
msgstr "Forslags F's støtter"

#: ../../english/template/debian/votebar.wml:82
msgid "Proposal G Seconds"
msgstr "Forslags G's støtter"

#: ../../english/template/debian/votebar.wml:85
msgid "Proposal H Seconds"
msgstr "Forslags H's støtter"

#: ../../english/template/debian/votebar.wml:88
msgid "Opposition"
msgstr "Opposition"

#: ../../english/template/debian/votebar.wml:91
msgid "Text"
msgstr "Tekst"

#: ../../english/template/debian/votebar.wml:94
msgid "Proposal A"
msgstr "Forslag A"

#: ../../english/template/debian/votebar.wml:97
msgid "Proposal B"
msgstr "Forslag B"

#: ../../english/template/debian/votebar.wml:100
msgid "Proposal C"
msgstr "Forslag C"

#: ../../english/template/debian/votebar.wml:103
msgid "Proposal D"
msgstr "Forslag D"

#: ../../english/template/debian/votebar.wml:106
msgid "Proposal E"
msgstr "Forslag E"

#: ../../english/template/debian/votebar.wml:109
msgid "Proposal F"
msgstr "Forslag F"

#: ../../english/template/debian/votebar.wml:112
msgid "Proposal G"
msgstr "Forslag G"

#: ../../english/template/debian/votebar.wml:115
msgid "Proposal H"
msgstr "Forslag H"

#: ../../english/template/debian/votebar.wml:118
msgid "Choices"
msgstr "Valgmuligheder"

#: ../../english/template/debian/votebar.wml:121
msgid "Amendment Proposer"
msgstr "Ændringens forslagstiller"

#: ../../english/template/debian/votebar.wml:124
msgid "Amendment Seconds"
msgstr "Ændringens støtter"

#: ../../english/template/debian/votebar.wml:127
msgid "Amendment Text"
msgstr "Ændringens tekst"

#: ../../english/template/debian/votebar.wml:130
msgid "Amendment Proposer A"
msgstr "Ændringens forslagstiller A"

#: ../../english/template/debian/votebar.wml:133
msgid "Amendment Seconds A"
msgstr "Ændringens støtter A"

#: ../../english/template/debian/votebar.wml:136
msgid "Amendment Text A"
msgstr "Ændringens tekst A"

#: ../../english/template/debian/votebar.wml:139
msgid "Amendment Proposer B"
msgstr "Ændringens forslagstiller B"

#: ../../english/template/debian/votebar.wml:142
msgid "Amendment Seconds B"
msgstr "Ændringens støtter B"

#: ../../english/template/debian/votebar.wml:145
msgid "Amendment Text B"
msgstr "Ændringens tekst B"

#: ../../english/template/debian/votebar.wml:148
msgid "Amendment Proposer C"
msgstr "Ændringens forslagstiller C"

#: ../../english/template/debian/votebar.wml:151
msgid "Amendment Seconds C"
msgstr "Ændringens støtter C"

#: ../../english/template/debian/votebar.wml:154
msgid "Amendment Text C"
msgstr "Ændringens tekst C"

#: ../../english/template/debian/votebar.wml:157
msgid "Amendments"
msgstr "Ændringsforslag"

#: ../../english/template/debian/votebar.wml:160
msgid "Proceedings"
msgstr "Forløb"

#: ../../english/template/debian/votebar.wml:163
msgid "Majority Requirement"
msgstr "Flertalskrav"

#: ../../english/template/debian/votebar.wml:166
msgid "Data and Statistics"
msgstr "Data og statistik"

#: ../../english/template/debian/votebar.wml:169
msgid "Quorum"
msgstr "Beslutningsdygtigt flertal"

#: ../../english/template/debian/votebar.wml:172
msgid "Minimum Discussion"
msgstr "Minimal diskussion"

#: ../../english/template/debian/votebar.wml:175
msgid "Ballot"
msgstr "Stemme"

#: ../../english/template/debian/votebar.wml:178
msgid "Forum"
msgstr "Forum"

#: ../../english/template/debian/votebar.wml:181
msgid "Outcome"
msgstr "Resultat"

#: ../../english/template/debian/votebar.wml:185
msgid "Waiting&nbsp;for&nbsp;Sponsors"
msgstr "Venter&nbsp;på&nbsp;en&nbsp;sponsor"

#: ../../english/template/debian/votebar.wml:188
msgid "In&nbsp;Discussion"
msgstr "Under&nbsp;diskussion"

#: ../../english/template/debian/votebar.wml:191
msgid "Voting&nbsp;Open"
msgstr "Afstemning&nbsp;åben"

#: ../../english/template/debian/votebar.wml:194
msgid "Decided"
msgstr "Besluttet"

#: ../../english/template/debian/votebar.wml:197
msgid "Withdrawn"
msgstr "Tilbagetrukket"

#: ../../english/template/debian/votebar.wml:200
msgid "Other"
msgstr "Andet"

#: ../../english/template/debian/votebar.wml:204
msgid "Home&nbsp;Vote&nbsp;Page"
msgstr "Hjem&nbsp;til&nbsp;afstemningssiden"

#: ../../english/template/debian/votebar.wml:207
msgid "How&nbsp;To"
msgstr "Hvordan&nbsp;man&nbsp;..."

#: ../../english/template/debian/votebar.wml:210
msgid "Submit&nbsp;a&nbsp;Proposal"
msgstr "Afgiver&nbsp;et&nbsp;forslag"

#: ../../english/template/debian/votebar.wml:213
msgid "Amend&nbsp;a&nbsp;Proposal"
msgstr "Ændrer&nbsp;et&nbsp;forslag"

#: ../../english/template/debian/votebar.wml:216
msgid "Follow&nbsp;a&nbsp;Proposal"
msgstr "Følger&nbsp;et&nbsp;forslag"

#: ../../english/template/debian/votebar.wml:219
msgid "Read&nbsp;a&nbsp;Result"
msgstr "Læser&nbsp;et&nbsp;resultat"

#: ../../english/template/debian/votebar.wml:222
msgid "Vote"
msgstr "Stemmer"
