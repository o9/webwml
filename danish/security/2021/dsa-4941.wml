#use wml::debian::translation-check translation="158226aad5fbf7d8645b13853a75c3562078bbc7" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder er opdaget i Linux-kernen, hvilke kunne føre til en 
rettighedsforøgelse, lammelsesangreb eller informationslækager.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36311">CVE-2020-36311</a>

    <p>En fejl blev opdaget i KVM-undersystemet til AMD-CPU'er, hvilket gjorde 
    det muligt for en angriber at forårsage et lammelsesangreb ved at udløse 
    destruktion af en stor SEV-VM.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3609">CVE-2021-3609</a>

    <p>Norbert Slusarek rapporterede om en kapløbstilstandssårbarhed i 
    netværksprotokollen CAN BCM, hvilken gjorde det muligt for en lokal angriber 
    at forøge rettigheder.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33909">CVE-2021-33909</a>

    <p>Qualys Research Labs opdagede en size_t-to-int-konverteringssårbarhed i 
    Linux-kernens filsystemlag.  En upriviligeret lokal angriber, som er i 
    stand til at oprette, mounte og dernæst slette en dyb mappestruktur, hvis 
    totale stilængde overstiger 1 GB, kunne drage nytte af fejlen til 
    rettighedsforøgelse.</p>

    <p>Flere oplysninger finder man i Qualys' bulletin på: 
    <a href="https://www.qualys.com/2021/07/20/cve-2021-33909/sequoia-local-privilege-escalation-linux.txt">\
    https://www.qualys.com/2021/07/20/cve-2021-33909/sequoia-local-privilege-escalation-linux.txt</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-34693">CVE-2021-34693</a>

    <p>Norbert Slusarek opdagede en informationslækage i netværksprotokollen CAN 
    BCM. En lokal angriber kunne drage nytte af fejlen til at få fat i følsomme 
    oplysninger fra kernestakhukommelsen.</p></li>

</ul>

<p>I den stabile distribution (buster), er disse problemer rettet i
version 4.19.194-3.</p>

<p>Vi anbefaler at du opgraderer dine linux-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende linux, se
dens sikkerhedssporingssidede på:
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4941.data"
